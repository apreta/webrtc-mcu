#!/bin/bash
runcmake() {
   cmake ../src
   echo "Done"
}
if [ -d /usr/local/opt/boost155 ]; then
    # Probably on Mac, use older Boost to make Ubuntu 14.04
    export BOOST_ROOT=/usr/local/opt/boost155
fi
BIN_DIR="build"
if [ -d $BIN_DIR ]; then
  cd $BIN_DIR
  runcmake
else
  mkdir $BIN_DIR
  cd $BIN_DIR
  runcmake
fi

