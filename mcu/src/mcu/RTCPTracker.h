/*
 * RTCPTracker.h
 */

#ifndef RTCPTRACKER_H_
#define RTCPTRACKER_H_

#include <string>
#include "logger.h"

namespace mcu {
  
class RTCPTracker {
  DECLARE_LOGGER();
public:
  RTCPTracker(bool outbound, const std::string& id, uint32_t ssrc);
  void checkRTP(char* buf, int len);
  void checkRTCP(char* buf, int len);

private:
  bool outbound_;
  std::string id_;
  uint32_t ssrc_;
  std::string logId_;
  
  uint16_t send_seq_;
  int rtp_reports_;
  int sent_bytes_;
};
  
  
  
}

#endif