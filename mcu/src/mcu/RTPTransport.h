#ifndef RTPTRANSPORT_H_
#define RTPTRANSPORT_H_

#include <string.h>
#include "RTPConnection.h"
#include "Transport.h"
#include "logger.h"

namespace mcu {
  class RTPTransport : public Transport, RTPConnectionListener, virtual RefCounter {
    DECLARE_LOGGER();

    public:
    RTPTransport(MediaType med, const std::string &transport_name, bool bundle, bool rtcp_mux, TransportListener *transportListener,
                 const std::string &stunServer, int stunPort, const std::string& localAddress, int minPort, int maxPort);
    virtual ~RTPTransport();
    void close();
    void connectionStateChanged(IceState newState);
    void onRTPData(char* data, int len, RTPConnection* nice);
    void onRTPClosed();
    void write(char* data, int len);
    void reset();
    void updateRTPState(RTPState state, RTPConnection *conn);
    void processLocalSdp(SdpInfo *localSdp_);

    // Dummy functions for Transport interface
    void onNiceData(unsigned int component_id, char* data, int len, NiceConnection* conn) { }
    void updateIceState(IceState state, NiceConnection *conn) { }

    private:
    bool readyRtp, readyRtcp;
    bool bundle_;
    friend class Transport;
  };
}
#endif
