/*
 * NiceConnection.cpp
 */

#include <glib.h>
#include <nice/nice.h>
#include <nice/address.h>
#include <cstdio>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "NiceConnection.h"
#include "SdpInfo.h"

namespace mcu {
  DEFINE_LOGGER(NiceConnection, "NiceConnection");

  class NiceConfig {
    std::string listen_ip;
    NiceAddress* listen_addr;
    std::string select_ips;
    bool logging;
    bool tcp;
    public:
      NiceConfig() {
        boost::property_tree::ptree pt;
        boost::property_tree::ini_parser::read_ini("nice.ini", pt);
        logging = pt.get<std::string>("nice.logging", "false") == "true";
        tcp = pt.get<std::string>("nice.tcp", "false") == "true";
        listen_ip = pt.get<std::string>("nice.listen_ip", "");
        listen_addr = nice_address_new();
        if (!nice_address_set_from_string(listen_addr, listen_ip.c_str())) {
          nice_address_free(listen_addr);
          listen_addr = NULL;
        }
        select_ips = pt.get<std::string>("nice.select_ips", "");
      }
    
      bool isLoggingEnabled() { return logging; }
      bool isTcpEnabled() { return tcp; }
      const std::string& getListenIP() { return listen_ip; }
      const std::string& getSelectIPs() { return select_ips; }
      NiceAddress* getListenAddress() { return listen_addr; }
  };
  static NiceConfig config;
  
  // Agent lock is released first
  void cb_nice_recv(NiceAgent* agent, guint stream_id, guint component_id,
      guint len, gchar* buf, gpointer user_data) {

    if (user_data==NULL||len==0)return;

    NiceConnection* nicecon = (NiceConnection*) user_data;
    nicecon->onData(component_id, reinterpret_cast<char*> (buf), static_cast<unsigned int> (len));
  }

  // Agent lock held
  void cb_candidate_gathering_done(NiceAgent *agent, guint stream_id,
      gpointer user_data) {

    NiceConnection *conn = (NiceConnection*) user_data;
    conn->gatheringDone(stream_id);
  }

  // Agent lock held
  void cb_component_state_changed(NiceAgent *agent, guint stream_id,
      guint component_id, guint state, gpointer user_data) {
    NiceConnection *conn = (NiceConnection*) user_data;
    conn->logState(component_id, state);
    if (state == NICE_COMPONENT_STATE_READY) {
      conn->updateComponentState(component_id, NICE_READY);
    } else if (state == NICE_COMPONENT_STATE_FAILED) {
      conn->updateIceState(NICE_FAILED);
    }
  }

  void cb_new_candidate(NiceAgent *agent, NiceCandidate *candidate,
      gpointer user_data) {
    NiceConnection *conn = (NiceConnection*) user_data;
    conn->newCandidate(false, candidate);
  }
  
  void cb_new_remote_candidate(NiceAgent *agent, NiceCandidate *candidate,
     gpointer user_data) {
    NiceConnection *conn = (NiceConnection*) user_data;
    conn->newCandidate(true, candidate);
  }
  
  // Agent lock held
  void cb_new_selected_pair(NiceAgent *agent, guint stream_id, guint component_id,
     gchar *lfoundation, gchar *rfoundation, gpointer user_data) {
    NiceConnection *conn = (NiceConnection*) user_data;
    conn->logPair(component_id, lfoundation, rfoundation);
  }

  gboolean cb_timer(gpointer user_data) {
    NiceConnection *conn = (NiceConnection*) user_data;
    conn->onTimer();
    return TRUE;
  }

  NiceConnection::NiceConnection(MediaType med,
      const std::string &transport_name, unsigned int iceComponents, const std::string& stunServer,
      int stunPort, int minPort, int maxPort):mediaType(med), iceComponents_(iceComponents),
  stunServer_(stunServer), stunPort_ (stunPort), minPort_(minPort), maxPort_(maxPort) {
    agent_ = NULL;
    loop_ = NULL;
    listener_ = NULL;
    context_ = NULL;
    localCandidates.reset(new std::vector<CandidateInfo>());
    transportName.reset(new std::string(transport_name));
    for (unsigned int i = 1; i<=iceComponents; i++) {
      comp_state_list[i] = NICE_INITIAL;
    }
  }

  NiceConnection::~NiceConnection() {
    ELOG_DEBUG("NiceConnection Destructor");
    m_Thread_.interrupt();
    m_Thread_.detach();
    /*if (!m_Thread_.try_join_for(boost::chrono::seconds(1))) {
      ELOG_ERROR("NiceConnection thread didn't exit, abandoning");
      m_Thread_.detach();
      return;
    }*/
  }

  void NiceConnection::close() {
    ELOG_DEBUG("NiceConnection close");
    // ToDo: Possible race if connection closed before fully established
    boost::mutex::scoped_lock lock(sendMutex_);
    if (iceState != NICE_FINISHED) {
      updateIceState(NICE_FINISHED);
      lock.unlock();
      if (loop_ != NULL) {
        if (g_main_loop_is_running(loop_)) {
          g_main_loop_quit(loop_);
        }
      }
    }
  }

  void NiceConnection::start() {
    add_ref();
    m_Thread_ = boost::thread(&NiceConnection::init, this);
  }

  /* WebRTCConnection sendLoop may still dispatch data after being closed so make
     sure we protect agent access with a mutex. */
  int NiceConnection::sendData(unsigned int compId, const void* buf, int len) {
    boost::mutex::scoped_lock lock(sendMutex_);
    int val = -1;
    if (iceState != NICE_READY) {
        return val;
    }

    val = nice_agent_send(agent_, 1, compId, len, reinterpret_cast<const gchar*>(buf));
    if (val != len) {
      ELOG_DEBUG("Data sent %d of %d", val, len);
    }
    return val;
  }

  void NiceConnection::onData(unsigned int compId, char *buf, unsigned int len) {
    getNiceListener()->onNiceData(compId, buf, len, this);
  }

  void NiceConnection::onTimer() {
    ELOG_DEBUG("Timer");
  }

  void NiceConnection::init() {

    this->updateIceState(NICE_INITIAL);

#if !GLIB_CHECK_VERSION(2,35,0)
    g_type_init();
#endif
    context_ = g_main_context_new();
    ELOG_DEBUG("Creating Main Loop");
    loop_ =  g_main_loop_new(context_, FALSE);
    if (config.isLoggingEnabled())
      nice_debug_enable( TRUE );
    else
      nice_debug_disable( TRUE );
    
    // Create a nice agent
    agent_ = nice_agent_new(context_, NICE_COMPATIBILITY_RFC5245);
    GValue enabled = { 0 };
    g_value_init(&enabled, G_TYPE_BOOLEAN);
    g_value_set_boolean(&enabled, false);
    g_object_set_property(G_OBJECT( agent_ ), "controlling-mode", &enabled);
    g_value_set_boolean(&enabled, true);
    g_object_set_property(G_OBJECT( agent_ ), "keepalive-conncheck", &enabled);
#ifdef ENABLE_TCP
    g_value_set_boolean(&enabled, config.isTcpEnabled());
    g_object_set_property(G_OBJECT( agent_ ), "ice-tcp", &enabled);
#endif

    //	NiceAddress* naddr = nice_address_new();
    //	nice_agent_add_local_address(agent_, naddr);
    
    if (config.getListenAddress()) {
      ELOG_DEBUG("Listening on %s", config.getListenIP().c_str());
      nice_agent_add_local_address(agent_, config.getListenAddress());
    }

    if (stunServer_.compare("") != 0 && stunPort_!=0){
      GValue val = { 0 }, val2 = { 0 };
      g_value_init(&val, G_TYPE_STRING);
      g_value_set_string(&val, stunServer_.c_str());
      g_object_set_property(G_OBJECT( agent_ ), "stun-server", &val);

      g_value_init(&val2, G_TYPE_UINT);
      g_value_set_uint(&val2, stunPort_);
      g_object_set_property(G_OBJECT( agent_ ), "stun-server-port", &val2);

      ELOG_WARN("Stun Server %s:%d", stunServer_.c_str(), stunPort_);
    }

    // Connect the signals
    g_signal_connect( G_OBJECT( agent_ ), "candidate-gathering-done",
        G_CALLBACK( cb_candidate_gathering_done ), this);
    g_signal_connect( G_OBJECT( agent_ ), "component-state-changed",
        G_CALLBACK( cb_component_state_changed ), this);
    g_signal_connect( G_OBJECT( agent_ ), "new-selected-pair",
        G_CALLBACK( cb_new_selected_pair ), this);
    g_signal_connect( G_OBJECT( agent_ ), "new-candidate-full",
        G_CALLBACK( cb_new_candidate ), this);
    g_signal_connect( G_OBJECT( agent_ ), "new-remote-candidate-full",
        G_CALLBACK( cb_new_remote_candidate ), this);

    // Create a new stream and start gathering candidates
    ELOG_DEBUG("Adding Stream... Number of components %d", iceComponents_);

    nice_agent_add_stream(agent_, iceComponents_);
    // Set Port Range ----> If this doesn't work when linking the file libnice.sym has to be modified to include this call
    if (minPort_!=0 && maxPort_!=0){
      nice_agent_set_port_range(agent_, (guint)1, (guint)1, (guint)minPort_, (guint)maxPort_);
    }

    nice_agent_gather_candidates(agent_, 1);
    nice_agent_attach_recv(agent_, 1, 1, context_,
        cb_nice_recv, this);
    if (iceComponents_ > 1) {
      nice_agent_attach_recv(agent_, 1, 2, context_,
          cb_nice_recv, this);
    }

#ifdef WATCH
    GSource *timer = g_timeout_source_new_seconds(2);
    g_source_set_callback(timer, cb_timer, this, NULL);
    g_source_attach(timer, context_);
#endif

    // Attach to the component to receive the data
    g_main_loop_run(loop_);

    if (agent_!=NULL){
      g_object_unref(agent_);
      agent_ = NULL;
    }
    if (loop_ != NULL) {
      g_main_loop_unref(loop_);
      loop_=NULL;
    }
    if (context_ != NULL) {
      g_main_context_unref(context_);
      context_ = NULL;
    }

    ELOG_DEBUG("Main Loop Exited");
    listener_->onNiceClosed();

    listener_->release();
    listener_ = NULL;

    release();
  }

  bool NiceConnection::setRemoteCandidates(
      std::vector<CandidateInfo> &candidates) {

    ELOG_DEBUG("Setting remote candidates %lu", candidates.size());


    for (unsigned int compId = 1; compId <= iceComponents_; compId++) {

      GSList* candList = NULL;

      for (unsigned int it = 0; it < candidates.size(); it++) {
        NiceCandidateType nice_cand_type;
        CandidateInfo cinfo = candidates[it];
        if (cinfo.mediaType != this->mediaType
            || cinfo.componentId != compId)
          continue;
        if (cinfo.tcpType != TCP_NONE && cinfo.tcpType != TCP_ACTIVE)
          continue;
#ifndef ENABLE_TCP
        if (cinfo.tcpType == TCP_PASSIVE)
          continue;
#endif

        switch (cinfo.hostType) {
          case HOST:
            nice_cand_type = NICE_CANDIDATE_TYPE_HOST;
            break;
          case SRLFX:
            nice_cand_type = NICE_CANDIDATE_TYPE_SERVER_REFLEXIVE;
            break;
          case PRFLX:
            nice_cand_type = NICE_CANDIDATE_TYPE_PEER_REFLEXIVE;
            break;
          case RELAY:
            nice_cand_type = NICE_CANDIDATE_TYPE_RELAYED;
            break;
          default:
            nice_cand_type = NICE_CANDIDATE_TYPE_HOST;
            break;
        }

        NiceCandidate* thecandidate = nice_candidate_new(nice_cand_type);
        NiceAddress* naddr = nice_address_new();
        nice_address_set_from_string(naddr, cinfo.hostAddress.c_str());
        nice_address_set_port(naddr, cinfo.hostPort);
        thecandidate->addr = *naddr;
        sprintf(thecandidate->foundation, "%s", cinfo.foundation.c_str());

        thecandidate->username = strdup(cinfo.username.c_str());
        thecandidate->password = strdup(cinfo.password.c_str());
        thecandidate->stream_id = (guint) 1;
        thecandidate->component_id = cinfo.componentId;
        thecandidate->priority = cinfo.priority;
#ifdef ENABLE_TCP
        if (cinfo.tcpType == TCP_ACTIVE)
          thecandidate->transport = NICE_CANDIDATE_TRANSPORT_TCP_ACTIVE;
        else
#endif
          thecandidate->transport = NICE_CANDIDATE_TRANSPORT_UDP;
        candList = g_slist_append(candList, thecandidate);
        ELOG_DEBUG("New Candidate SET %s %s %d", cinfo.transProtocol.c_str(), cinfo.hostAddress.c_str(), cinfo.hostPort);
      }

      nice_agent_set_remote_candidates(agent_, (guint) 1, compId, candList);

      GSList* iterator;
      for (iterator = candList; iterator; iterator = iterator->next) {
        NiceCandidate *cand = (NiceCandidate*) iterator->data;
        nice_candidate_free(cand);
      }
      g_slist_free(candList);
    }

    ELOG_DEBUG("Candidates SET");
    this->updateIceState(NICE_CANDIDATES_RECEIVED);
    return true;
  }

  void NiceConnection::gatheringDone(uint stream_id) {
    int currentCompId = 1;
    GSList* lcands;
    lcands = nice_agent_get_local_candidates(agent_, stream_id, currentCompId++);
    NiceCandidate *cand;
    GSList* iterator;
    gchar *ufrag = NULL, *upass = NULL;
    nice_agent_get_local_credentials(agent_, stream_id, &ufrag, &upass);

    //	ELOG_DEBUG("gathering done %u",stream_id);
    //ELOG_DEBUG("Candidates---------------------------------------------------->");
    while (lcands != NULL) {
      for (iterator = lcands; iterator; iterator = iterator->next) {
        char address[40], baseAddress[40];
        cand = (NiceCandidate*) iterator->data;
        nice_address_to_string(&cand->addr, address);
        nice_address_to_string(&cand->base_addr, baseAddress);
        if (strstr(address, ":") != NULL) {
          ELOG_DEBUG("Ignoring IPV6 candidate %s", address);
          continue;
        }
        //			ELOG_DEBUG("foundation %s", cand->foundation);
        //			ELOG_DEBUG("compid %u", cand->component_id);
        //			ELOG_DEBUG("stream_id %u", cand->stream_id);
        //			ELOG_DEBUG("priority %u", cand->priority);
        //			ELOG_DEBUG("username %s", cand->username);
        //			ELOG_DEBUG("password %s", cand->password);
        CandidateInfo cand_info;
        cand_info.componentId = cand->component_id;
        cand_info.foundation = cand->foundation;
        cand_info.priority = cand->priority;
        cand_info.hostAddress = std::string(address);
        cand_info.hostPort = nice_address_get_port(&cand->addr);
        cand_info.mediaType = mediaType;

        switch (cand->type) {
          case NICE_CANDIDATE_TYPE_HOST:
            cand_info.hostType = HOST;
            // Ignore host IP addresses that haven't been selected
            if (!config.getSelectIPs().empty() && config.getSelectIPs().find(address) == std::string::npos) {
              ELOG_DEBUG("Ignoring candidate %s", address);
              continue;
            }
            break;
          case NICE_CANDIDATE_TYPE_SERVER_REFLEXIVE:
            cand_info.hostType = SRLFX;
            cand_info.baseAddress = std::string(baseAddress);
            cand_info.basePort = nice_address_get_port(&cand->base_addr);
            break;
          case NICE_CANDIDATE_TYPE_PEER_REFLEXIVE:
            cand_info.hostType = PRFLX;
            break;
          case NICE_CANDIDATE_TYPE_RELAYED:
            ELOG_DEBUG("WARNING TURN NOT IMPLEMENTED YET");
            cand_info.hostType = RELAY;
            break;
          default:
            break;
        }
        switch (cand->transport) {
          case NICE_CANDIDATE_TRANSPORT_UDP:
            cand_info.netProtocol = "udp";
            cand_info.tcpType = TCP_NONE;
            break;
#ifdef ENABLE_TCP
          case NICE_CANDIDATE_TRANSPORT_TCP_PASSIVE:
            cand_info.netProtocol = "tcp";
            cand_info.tcpType = TCP_PASSIVE;
            break;
#endif
          default:
            ELOG_DEBUG("Ignoring candidate %s with transport %d", address, cand->transport);
            continue;
        }
        cand_info.transProtocol = std::string(*transportName.get());
        

        cand_info.username = std::string(ufrag);

        cand_info.password = std::string(upass);
        /*
           if (cand->username)
           cand_info.username = std::string(cand->username);
           else
           cand_info.username = std::string("(null)");

           if (cand->password)
           cand_info.password = std::string(cand->password);
           else
           cand_info.password = std::string("(null)");
           */

        localCandidates->push_back(cand_info);
      }
      lcands = nice_agent_get_local_candidates(agent_, stream_id,
          currentCompId++);
    }
    ELOG_INFO("candidate_gathering done with %lu candidates", localCandidates->size());

    if (localCandidates->size()==0){
      ELOG_WARN("No local candidates found, check your network connection");
      exit(0);
    }
    updateIceState(NICE_CANDIDATES_GATHERED);
  }

  void NiceConnection::setNiceListener(NiceConnectionListener *listener) {
    // Maybe we could use boost::intrusive_ptr...
    listener->add_ref();
    if (listener_)
        listener_->release();
    listener_ = listener;
  }

  NiceConnectionListener* NiceConnection::getNiceListener() {
    return this->listener_;
  }

  void NiceConnection::updateComponentState(unsigned int compId, IceState state) {
    ELOG_DEBUG("%s - NICE Component State Changed %u - %u", transportName->c_str(), compId, state);
    comp_state_list[compId] = state;
    if (state == NICE_READY) {
      for (unsigned int i = 1; i<=iceComponents_; i++) {
        if (comp_state_list[i] != NICE_READY) {
          return;
        }
      }
    }
    this->updateIceState(state);
  }

  void NiceConnection::updateIceState(IceState state) {
    ELOG_DEBUG("%s - NICE State Changed %u", transportName->c_str(), state);
    this->iceState = state;
    if (this->listener_ != NULL) {
      this->listener_->updateIceState(state, this);
    }
  }

  void NiceConnection::newCandidate(bool remote, NiceCandidate *candidate) {
    char addr[256];
    nice_address_to_string(&candidate->addr, addr);
    ELOG_DEBUG("New %s candidate: %s trans %u type %u", remote ? "remote" : "local",
               addr, candidate->transport, candidate->type);
  }

  void NiceConnection::logPair(unsigned int compId, const char *lfound, const char *rfound) {
    ELOG_DEBUG("%s - NICE pair selected %u: %s - %s", transportName->c_str(), compId, lfound, rfound);
  }
  
  void NiceConnection::logState(unsigned int compId, unsigned int state) {
    ELOG_DEBUG("%s - NICE internal state %u - %u", transportName->c_str(), compId, state);
  }
  
} /* namespace mcu */
