/*
 * OneToManyProcessor.h
 */

#ifndef ONETOMANYPROCESSOR_H_
#define ONETOMANYPROCESSOR_H_

#include <map>
//#include <unordered_map> too new for ubuntu 12.04
#include <string>

#include "MediaDefinitions.h"
#include "media/ExternalOutput.h"
#include "RefCount.h"
#include "logger.h"

namespace mcu{

class WebRtcConnection;

/**
 * Represents a One to Many connection.
 * Receives media from one publisher and retransmits it to every subscriber.
 */
class OneToManyProcessor : public MediaSink, public FeedbackSink, virtual public RefCounter {
	DECLARE_LOGGER();

public:
	MediaSource *publisher;
	std::map<std::string, MediaSink*> subscribers;
	std::map<std::string, bool> pausedVideo;
	std::map<std::string, bool> pausedAudio;

	OneToManyProcessor();
	virtual ~OneToManyProcessor();

	void close();

	/**
	 * Sets the Publisher
	 * @param webRtcConn The WebRtcConnection of the Publisher
	 */
	void setPublisher(MediaSource* webRtcConn);
	/**
	 * Sets the subscriber
	 * @param webRtcConn The WebRtcConnection of the subscriber
	 * @param peerId An unique Id for the subscriber
	 */
	void addSubscriber(MediaSink* webRtcConn, const std::string& peerId);
	/**
	 * Eliminates the subscriber given its peer id
	 * @param peerId the peerId
	 */
	void removeSubscriber(const std::string& peerId);
	/**
	 * Pause or unpause video stream to indicated peer.
	 * @param peerId the peerId
	 */ 
	void pauseVideo(const std::string& peerId, bool enable);
	/**
	 * Pause or unpause audio stream to indicated peer.
	 * @param peerId the peerId
	 */
	void pauseAudio(const std::string& peerId, bool enable);

    std::string monitorStream(const std::string& peerId, const std::string& op);

	int deliverAudioData(char* buf, int len);
	int deliverVideoData(char* buf, int len);

	int deliverFeedback(char* buf, int len);

private:
	//char sendVideoBuffer_[2000];
	//char sendAudioBuffer_[2000];
    boost::recursive_mutex mutex_;
	unsigned int sentPackets_;
	std::string rtcpReceiverPeerId_;
	FeedbackSink* feedbackSink_;
	ExternalOutput* recorder_;
	bool closed_;
	void closeAll();
};

} /* namespace mcu */
#endif /* ONETOMANYPROCESSOR_H_ */
