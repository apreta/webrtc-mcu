/*
 * OneToManyProcessor.cpp
 */

#include "OneToManyProcessor.h"
#include "WebRtcConnection.h"
#include "rtputils.h"

namespace mcu {
  DEFINE_LOGGER(OneToManyProcessor, "OneToManyProcessor");
  OneToManyProcessor::OneToManyProcessor() {
    publisher = NULL;
    feedbackSink_ = NULL;
    sentPackets_ = 0;
    closed_ = false;
  }

  OneToManyProcessor::~OneToManyProcessor() {
    ELOG_DEBUG ("OneToManyProcessor destructor");
    feedbackSink_ = NULL;
    std::map<std::string, MediaSink*>::iterator it;
    for (it = subscribers.begin(); it != subscribers.end(); ++it) {
      //delete (*it).second;
      (*it).second->release();
    }
    //delete this->publisher;
    this->publisher->release();
  }

  void OneToManyProcessor::close() {
    ELOG_DEBUG ("OneToManyProcessor close");
    this->closeAll();
  }

  int OneToManyProcessor::deliverAudioData(char* buf, int len) {
    boost::recursive_mutex::scoped_lock lock(mutex_);
    if (subscribers.empty() || len <= 0 || closed_)
      return 0;

    std::map<std::string, MediaSink*>::iterator it;
    for (it = subscribers.begin(); it != subscribers.end(); it++) {
      std::map<std::string, bool>::iterator it2 = pausedAudio.find((*it).first);
      if (it2 != pausedAudio.end() && (*it2).second == true)
        continue;
      (*it).second->deliverAudioData(buf, len);
    }

    return 0;
  }

  int OneToManyProcessor::deliverVideoData(char* buf, int len) {
    boost::recursive_mutex::scoped_lock lock(mutex_);
    if (subscribers.empty() || len <= 0 || closed_)
      return 0;

    rtcpheader* head = reinterpret_cast<rtcpheader*>(buf);
    if(head->packettype==RTCP_Receiver_PT || head->packettype==RTCP_PS_Feedback_PT || head->packettype == RTCP_RTP_Feedback_PT){
      ELOG_WARN("Receiving Feedback in wrong path: %d", head->packettype);
      if (feedbackSink_){
        head->ssrc = htonl(publisher->getVideoSourceSSRC());
        feedbackSink_->deliverFeedback(buf,len);
      }
      return 0;
    }
    std::map<std::string, MediaSink*>::iterator it;
    for (it = subscribers.begin(); it != subscribers.end(); it++) {
      if ((head->packettype & 0x7f) == VP8_90000_PT || (head->packettype & 0x7f) == RED_90000_PT) {
        std::map<std::string, bool>::iterator it2 = pausedVideo.find((*it).first);
        if (it2 != pausedVideo.end() && (*it2).second == true)
          continue;
      }
      (*it).second->deliverVideoData(buf, len);
    }
    sentPackets_++;
    return 0;
  }

  void OneToManyProcessor::setPublisher(MediaSource* webRtcConn) {
    ELOG_DEBUG("SET PUBLISHER");
    boost::recursive_mutex::scoped_lock lock(mutex_);
    this->publisher = webRtcConn;
    feedbackSink_ = publisher->getFeedbackSink();
    // We're taking over ownership so no add_ref here
  }

  int OneToManyProcessor::deliverFeedback(char* buf, int len){
    boost::recursive_mutex::scoped_lock lock(mutex_);
    if (feedbackSink_ != NULL && !closed_) {
      feedbackSink_->deliverFeedback(buf,len);
    }
    return 0;
  }

  void OneToManyProcessor::addSubscriber(MediaSink* webRtcConn,
      const std::string& peerId) {
    ELOG_DEBUG("Adding subscriber");
    ELOG_DEBUG("From %u, %u ", publisher->getAudioSourceSSRC() , publisher->getVideoSourceSSRC());
    boost::recursive_mutex::scoped_lock lock(mutex_);
    webRtcConn->setAudioSinkSSRC(this->publisher->getAudioSourceSSRC());
    webRtcConn->setVideoSinkSSRC(this->publisher->getVideoSourceSSRC());
    ELOG_DEBUG("Subscribers ssrcs: Audio %u, video, %u from %u, %u ", webRtcConn->getAudioSinkSSRC(), webRtcConn->getVideoSinkSSRC(), this->publisher->getAudioSourceSSRC() , this->publisher->getVideoSourceSSRC());
    FeedbackSource* fbsource = webRtcConn->getFeedbackSource();

    if (fbsource!=NULL){
      ELOG_DEBUG("adding fbsource");
      fbsource->setFeedbackSink(this);
    }
    this->subscribers[peerId] = webRtcConn;
    // taking over ownership, no add_ref here
  }

  void OneToManyProcessor::removeSubscriber(const std::string& peerId) {
    boost::recursive_mutex::scoped_lock lock(mutex_);
    if (this->subscribers.find(peerId) != subscribers.end()) {
      ELOG_DEBUG("Removing subscriber %s", peerId.c_str());
      //delete this->subscribers[peerId];
      this->subscribers[peerId]->close();
      this->subscribers[peerId]->release();
      this->subscribers.erase(peerId);
    }
  }

  void OneToManyProcessor::pauseVideo(const std::string& peerId, bool enable) {
	ELOG_WARN("Pause video for %s is %s", peerId.c_str(), enable ? "enabled" : "disabled");
    boost::recursive_mutex::scoped_lock lock(mutex_);
	this->pausedVideo[peerId] = enable;
    if (!enable && this->subscribers.find(peerId) != subscribers.end()) {
      this->subscribers[peerId]->resetVideoStream();
    }
  }

  void OneToManyProcessor::pauseAudio(const std::string& peerId, bool enable) {
    ELOG_WARN("Pause audio for %s is %s", peerId.c_str(), enable ? "enabled" : "disabled");
    boost::recursive_mutex::scoped_lock lock(mutex_);
    this->pausedAudio[peerId] = enable;
    if (!enable && this->subscribers.find(peerId) != subscribers.end()) {
        this->subscribers[peerId]->resetAudioStream();
    }
  }

  std::string OneToManyProcessor::monitorStream(const std::string& peerId, const std::string& op) {
      ELOG_WARN("Monitor stream for %s: %s", peerId.size() == 0 ? "publisher" : peerId.c_str(), op.c_str());
      std::string res;
      boost::recursive_mutex::scoped_lock lock(mutex_);
      if (peerId.size() > 0) {
          // monitor subscription
          if (this->subscribers.find(peerId) != subscribers.end()) {
          }
      } else {
          // monitor publisher
      }
      return res;
  }

  void OneToManyProcessor::closeAll() {
    ELOG_DEBUG ("OneToManyProcessor closeAll");
    boost::recursive_mutex::scoped_lock lock(mutex_);
    std::map<std::string, MediaSink*>::iterator it;
    for (it = subscribers.begin(); it != subscribers.end(); ++it) {
//      (*it).second->closeSink();
      (*it).second->close();
    }
    this->publisher->close();
    closed_ = true;
  }

}/* namespace mcu */

