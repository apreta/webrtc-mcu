/*
 * RTPTransport.cpp
 */
#include <iostream>
#include <cassert>
#include <glib.h>

#include "RTPTransport.h"
#include "RTPConnection.h"

#include "rtputils.h"

using namespace mcu;
using namespace std;

DEFINE_LOGGER(RTPTransport, "RTPTransport");

RTPTransport::RTPTransport(MediaType med, const std::string &transport_name, bool bundle, bool rtcp_mux, TransportListener *transportListener,
      const std::string &stunServer, int stunPort, const std::string &localAddress, int minPort, int maxPort) :
      Transport(med, transport_name, bundle, rtcp_mux, transportListener, stunServer, stunPort, minPort, maxPort) {
    ELOG_DEBUG("Initializing RTPTransport")
    updateTransportState(TRANSPORT_INITIAL);

    readyRtp = false;
    readyRtcp = false;

    bundle_ = bundle;

    rtp_ = new RTPConnection(med, transport_name, stunServer, stunPort, localAddress, minPort, maxPort);
    rtp_->setRTPListener(this);
    rtp_->start();
}

RTPTransport::~RTPTransport() {
  ELOG_DEBUG("RTPTransport Destructor");
  // delete rtp_;

}

void RTPTransport::close() {
  if (rtp_) rtp_->close();
  updateTransportState(TRANSPORT_FINISHED);
}

void RTPTransport::onRTPData(char* data, int len, RTPConnection* conn) {
    if (this->getTransportState() == TRANSPORT_READY) {
      if (len <= 0)
          return;

      getTransportListener()->onTransportData(data, len, this);
    }
}

void RTPTransport::onRTPClosed() {
    rtp_->release();
    onListenerDone();
}

void RTPTransport::write(char* data, int len) {
    int comp = 1;
    if (this->getTransportState() == TRANSPORT_READY) {
      rtcpheader *chead = reinterpret_cast<rtcpheader*> (data);
      if (chead->packettype == RTCP_Sender_PT || chead->packettype == RTCP_Receiver_PT || chead->packettype == RTCP_PS_Feedback_PT
          || chead->packettype == RTCP_RTP_Feedback_PT) {
        if (!rtcp_mux_) {
          comp = 2;
        }
      }
      else{
        comp = 1;
      }
      if (len <= 10) {
        return;
      }
      if (rtp_->rtpState == RTP_READY) {
        rtp_->sendData(comp, data, len);
      }
    }
}

void RTPTransport::reset() {

	if (readyRtp) {
        ELOG_DEBUG("Resetting RTP rtp params");

        readyRtp = true;
        readyRtcp = true;
    }
}

void RTPTransport::updateRTPState(RTPState state, RTPConnection *conn) {
    ELOG_DEBUG( "New RTP state %d %d %d" , state , mediaType , bundle_ );
    if (state == RTP_INITIAL) {
      updateTransportState(TRANSPORT_STARTED);
    }
    if (state == RTP_READY && !readyRtp) {
        ELOG_DEBUG("Setting RTP params");
        readyRtp = true;
        readyRtcp = true;
        if (readyRtp && readyRtcp) {
          updateTransportState(TRANSPORT_READY);
        }
    }
}

void RTPTransport::processLocalSdp(SdpInfo *localSdp_) {
  ELOG_DEBUG( "Processing Local SDP in RTP Transport" );
  if (rtp_->rtpState >= RTP_INITIAL) {
    // set rtp audio rtp
    CandidateInfo info;
    info.hostType = NONE;
    info.hostPort = rtp_->localPort;
    info.hostAddress = rtp_->localAddress;
    info.mediaType = rtp_->mediaType;
    info.isBundle = false;
    localSdp_->addCandidate(info);
    localSdp_->isIceConnection = false;
  }
  ELOG_DEBUG( "Processed Local SDP in RTP Transport" );
}
