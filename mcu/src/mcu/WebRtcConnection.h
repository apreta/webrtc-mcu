#ifndef WEBRTCCONNECTION_H_
#define WEBRTCCONNECTION_H_

#include <string>
#include <queue>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

#include "logger.h"

#include "SrtpChannel.h"
#include "SdpInfo.h"
#include "MediaDefinitions.h"
#include "Transport.h"

namespace mcu {

class Transport;
class TransportListener;
class RTCPTracker;

/**
 * States of ICE
 */
enum WebRTCState {
	INITIAL, STARTED, READY, FINISHED, FAILED
};

class WebRtcConnectionStateListener {
public:
	virtual ~WebRtcConnectionStateListener() { }
	virtual void connectionStateChanged(WebRTCState newState)=0;
};

struct BitrateReport {
    long long last_received;
    unsigned int bitrate;

    BitrateReport() : last_received(0), bitrate(0)
    { }
    BitrateReport(long long last_ts, unsigned int br) :
            last_received(last_ts), bitrate(br)
    { }
};

    /**
 * A WebRTC Connection. This class represents a WebRTC Connection that can be established with other peers via a SDP negotiation
 * it comprises all the necessary Transport components.
 */
class WebRtcConnection: public MediaSink, public MediaSource, public FeedbackSink, public FeedbackSource, public TransportListener, public RefCounter {
	DECLARE_LOGGER();
public:
	/**
	 * Constructor.
	 * Constructs an empty WebRTCConnection without any configuration.
	 */
	WebRtcConnection(bool audioEnabled, bool videoEnabled, const std::string &stunServer, int stunPort,
                     const std::string &localAddress, int minPort, int maxPort);
	/**
	 * Destructor.
	 */
	virtual ~WebRtcConnection();
	/**
	 * Inits the WebConnection by starting ICE Candidate Gathering.
	 * @return True if the candidates are gathered.
	 */
	bool init();

	void close();

	/**
	 * Sets the SDP of the remote peer.
	 * @param sdp The SDP.
	 * @return true if the SDP was received correctly.
	 */
	bool setRemoteSdp(const std::string &sdp);
	/**
	 * Obtains the local SDP.
	 * @return The SDP as a string.
	 */
	std::string getLocalSdp();

	int deliverAudioData(char* buf, int len);
	int deliverVideoData(char* buf, int len);
	void resetVideoStream();
	void resetAudioStream();

    int deliverFeedback(char* buf, int len);

	/**
	 * Sends a FIR Packet (RFC 5104) asking for a keyframe
	 * @return the size of the data sent
	 */
	int sendFirPacket();

	void setWebRTCConnectionStateListener(
			WebRtcConnectionStateListener* listener);
	/**
	 * Gets the current state of the Ice Connection
	 * @return
	 */
	WebRTCState getCurrentState();

	void onTransportData(char* buf, int len, Transport *transport);

	void updateState(TransportState state, Transport * transport);

	void queueData(int comp, const char* data, int len, Transport *transport);

    void getStats(unsigned int &sent, unsigned int &received);

private:
	SdpInfo remoteSdp_;
	SdpInfo localSdp_;

	WebRTCState globalState_;

	int video_, audio_, bundle_, sequenceNumberFIR_;
	boost::mutex queueMutex_;
	boost::thread send_Thread_;
	std::queue<dataPacket> sendQueue_;
	WebRtcConnectionStateListener* connStateListener_;
	Transport *videoTransport_, *audioTransport_;
	char deliverMediaBuffer_[3000];

	bool sending_;

	void sendLoop();
	void writeSsrc(char* buf, int len, unsigned int ssrc);
	void processRtcpHeaders(char* buf, int len, unsigned int ssrc);
    void processRembHeader(void* buf, unsigned int ssrc);
    void processRRHeader(void* buf, unsigned int ssrc);
    void processSRHeader(void* buf);
	void writePayloadType(char* buf, int len);
    unsigned int findLowestBitrate();

	bool audioEnabled_;
	bool videoEnabled_;

	bool resetVideo_;
	bool resetAudio_;

	int stunPort_, minPort_, maxPort_;
	std::string stunServer_;
    std::string localAddress_;

	boost::condition_variable cond_;

    boost::atomic<unsigned int> sentBytes_;
    boost::atomic<unsigned int> receivedBytes_;

    bool filterRtcp_;
    unsigned int minBitrate_;
    std::map<unsigned int, BitrateReport> receiverReports_;

    unsigned int lastSrSentTs_;
    unsigned int lastSrSentPackets_;
    long long lastSrRecTs_;
    unsigned int seqEpoch_;
    unsigned int highestSeq_;

    RTCPTracker *rtcpTracker_;
};

} /* namespace mcu */
#endif /* WEBRTCCONNECTION_H_ */
