#ifndef TRANSPORT_H_
#define TRANSPORT_H_

#include <string.h>
#include <cstdio>
#include "NiceConnection.h"
#include "RTPConnection.h"

/**
 * States of Transport
 */
enum TransportState {
  TRANSPORT_INITIAL, TRANSPORT_STARTED, TRANSPORT_READY, TRANSPORT_FINISHED, TRANSPORT_FAILED
};

namespace mcu {
  class Transport;

  class TransportListener : public virtual RefCounted {
    public:
      virtual void onTransportData(char* buf, int len, Transport *transport) = 0;
      virtual void queueData(int comp, const char* data, int len, Transport *transport) = 0;
      virtual void updateState(TransportState state, Transport *transport) = 0;
  };

  class Transport : public virtual RefCounted {
    public:
      NiceConnection *nice_;
      RTPConnection *rtp_;
      MediaType mediaType;
      std::string transport_name;
      Transport(MediaType med, const std::string &transport_name, bool bundle, bool rtcp_mux, TransportListener *transportListener, const std::string &stunServer, int stunPort, int minPort, int maxPort) {
        this->transport_name = transport_name;
        this->mediaType = med;
        this->stunServer_ = stunServer;
        this->stunPort_ = stunPort;
        this->minPort_ = minPort;
        this->maxPort_ = maxPort;
        transpListener_ = transportListener;
        transpListener_->add_ref();
        rtcp_mux_ = rtcp_mux;
        nice_ = NULL;
        rtp_ = NULL;
      }
      virtual ~Transport(){};
      virtual void close() = 0;
      virtual void write(char* data, int len) = 0;
      virtual void reset() = 0;
      virtual void processLocalSdp(SdpInfo *localSdp_) = 0;
      void onListenerDone() { transpListener_->release(); }
      void setTransportListener(TransportListener * listener) {
        listener->add_ref();
        if (transpListener_) transpListener_->release();
        transpListener_ = listener;
      }
      TransportListener* getTransportListener() {
        return transpListener_;
      }
      TransportState getTransportState() {
        return state_;
      }
      void updateTransportState(TransportState state) {
        if (state == state_) {
          return;
        }
        state_ = state;
        if (transpListener_ != NULL) {
          transpListener_->updateState(state, this);
        }
      }
      NiceConnection* getNiceConnection() {
        return nice_;
      }
      void writeOnNice(int comp, void* buf, int len) {
        getNiceConnection()->sendData(comp, buf, len);
      }
      // Adding in a non-OO form to avoid changing class hierarchies
      RTPConnection* getRTPConnection() {
        return rtp_;
      }
      void writeOnRTP(int comp, void* buf, int len) {
        getRTPConnection()->sendData(comp, buf, len);
      }
      bool setRemoteCandidates(std::vector<CandidateInfo> &candidates) {
        if (getNiceConnection()) {
          return getNiceConnection()->setRemoteCandidates(candidates);
        }
        return false;
      }
      bool rtcp_mux_;
    private:
      TransportListener *transpListener_;

      boost::atomic<TransportState> state_;

      int stunPort_, minPort_, maxPort_;

      std::string stunServer_;

      friend class NiceConnection;
  };

};
#endif
