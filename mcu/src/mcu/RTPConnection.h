/*
 * RTPConnection.h
 */

#ifndef RTPCONNECTION_H_
#define RTPCONNECTION_H_

#include <string>
#include <vector>
#include <boost/thread.hpp>

#include "MediaDefinitions.h"
#include "SdpInfo.h"
#include "logger.h"
#include "NiceConnection.h"

typedef struct _GCancellable GCancellable;
typedef struct _GSocket GSocket;
typedef struct _GSocketAddress GSocketAddress;

typedef unsigned int uint;

namespace mcu {
//forward declarations
class WebRtcConnection;
class RTPConnection;


enum RTPState {
  RTP_INITIAL, RTP_READY, RTP_FINISHED, RTP_FAILED
};

class RTPConnectionListener : public virtual RefCounted  {
public:
  virtual void onRTPData(char* data, int len, RTPConnection* conn)=0;
  virtual void updateRTPState(RTPState state, RTPConnection* conn)=0;
  virtual void onRTPClosed() = 0;
};

/**
 * An RTP connection.
 *
 */
class RTPConnection : public RefCounter {
	DECLARE_LOGGER();
public:

	/**
	 * Constructs a new RTPConnection.
	 * @param med The MediaType of the connection.
	 * @param transportName The name of the transport protocol. Was used when WebRTC used video_rtp instead of just rtp.
   * @param iceComponents Number of ice components pero connection. Default is 1 (rtcp-mux).
	 */
	RTPConnection(MediaType med, const std::string &transportName, const std::string &remoteServer, uint remotePort,
                  const std::string &localAddress, int minPort = 0, int maxPort = 65535);

	virtual ~RTPConnection();

	void close();

	/**
	 * Starts Gathering candidates in a new thread.
	 */
	void start();

	/**
	 * Sets the associated Listener.
	 * @param connection Pointer to the NiceConnectionListener.
	 */
	void setRTPListener(RTPConnectionListener *listener);

	/**
	 * Gets the associated Listener.
	 * @param connection Pointer to the NiceConnectionListener.
	 */
	RTPConnectionListener* getRTPListener();

	/**
	 * Sends data via the RTP Connection.
	 * @param buf Pointer to the data buffer.
	 * @param len Length of the Buffer.
	 * @return Bytes sent.
	 */
	int sendData(unsigned int compId, const void* buf, int len);

	/**
	 * The MediaType of the connection
	 */
	MediaType mediaType;
	/**
	 * The transport name
	 */
	boost::scoped_ptr<std::string> transportName;
	/**
	 * The state of the ice Connection
	 */
	boost::atomic<RTPState> rtpState;

	std::string localAddress;
	uint localPort;

	void updateRTPState(RTPState state);


private:
	void init();
	RTPConnectionListener* listener_;
	GCancellable* cancellable_;
	GSocket* socket_;
	boost::thread m_Thread_;
    boost::recursive_mutex mutex_;
	GSocketAddress* remoteAddress_;
	int minPort_, maxPort_;
};

} /* namespace mcu */
#endif /* RTPCONNECTION_H_ */
