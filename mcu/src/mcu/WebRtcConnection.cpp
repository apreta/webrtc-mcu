/*
 * WebRTCConnection.cpp
 */

#include <cstdio>
#include <cstdlib>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "WebRtcConnection.h"
#include "DtlsTransport.h"
#include "SdesTransport.h"
#include "RTPTransport.h"

#include "SdpInfo.h"
#include "rtputils.h"
#include "RTCPTracker.h"
#include "rtcputils.h"

namespace mcu {
  DEFINE_LOGGER(WebRtcConnection, "WebRtcConnection");

  long long current_timestamp() {
      struct timeval te;
      gettimeofday(&te, NULL); // get current time
      long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
      return milliseconds;
  }

  class WebRtcConfig {
  public:
    bool filterHighBWRtcp;
    bool filterAllRtcp;
    unsigned int minBitRate;
    unsigned int highBWThreshold;
    unsigned int minHighBWBitRate;
    WebRtcConfig() {
      boost::property_tree::ptree pt;
      boost::property_tree::ini_parser::read_ini("webrtc.ini", pt);
      filterHighBWRtcp = pt.get<std::string>("webrtc.filter_high_bandwidth", "false") == "true";
      filterAllRtcp = pt.get<std::string>("webrtc.filter_all", "false") == "true";
      minBitRate = pt.get<unsigned int>("webrtc.min_bitrate", 0);
      highBWThreshold = pt.get<unsigned int>("webrtc.high_bw_threshold", 500000);
      minHighBWBitRate = pt.get<unsigned int>("webrtc.high_bw_min_bitrate", 200000);
    }
  };
  static WebRtcConfig config;


  WebRtcConnection::WebRtcConnection(bool audioEnabled, bool videoEnabled, const std::string &stunServer, int stunPort,
                                     const std::string &localAddress, int minPort, int maxPort) {

    ELOG_WARN("WebRtcConnection constructor stunserver %s stunPort %d minPort %d maxPort %d", stunServer.c_str(), stunPort, minPort, maxPort);
    video_ = 0;
    audio_ = 0;
    sequenceNumberFIR_ = 0;
    bundle_ = false;
    this->setVideoSinkSSRC(55543);
    this->setAudioSinkSSRC(44444);
    videoSink_ = NULL;
    audioSink_ = NULL;
    fbSink_ = NULL;
    sourcefbSink_ = this;
    sinkfbSource_ = this;

    globalState_ = INITIAL;
    connStateListener_ = NULL;

    add_ref();
    sending_ = true;
    send_Thread_ = boost::thread(&WebRtcConnection::sendLoop, this);

    videoTransport_ = NULL;
    audioTransport_ = NULL;

    audioEnabled_ = audioEnabled;
    videoEnabled_ = videoEnabled;

    resetVideo_ = false;

    stunServer_ = stunServer;
    stunPort_ = stunPort;
    localAddress_ = localAddress;
    minPort_ = minPort;
    maxPort_ = maxPort;

    receivedBytes_ = 0;
    sentBytes_ = 0;

    filterRtcp_ = false;
    minBitrate_ = 0;

    lastSrSentTs_ = 0;
    lastSrSentPackets_ = 0;
    lastSrRecTs_ = 0;
    seqEpoch_ = 0;
    highestSeq_ = 0;

    rtcpTracker_ = NULL;
  }

  WebRtcConnection::~WebRtcConnection() {
    ELOG_DEBUG("WebRtcConnection Destructor");
    send_Thread_.interrupt();
    send_Thread_.detach();
    /*if (!send_Thread_.try_join_for(boost::chrono::seconds(1))) {
      ELOG_ERROR("WebRtcConnection thread didn't exit, abandoning");
      send_Thread_.detach();
      return;
    }*/
    videoSink_ = NULL;
    audioSink_ = NULL;
    fbSink_ = NULL;
    //delete videoTransport_;
    if (videoTransport_) {
      videoTransport_->release();
      videoTransport_=NULL;
    }
    //delete audioTransport_;
    if (audioTransport_) {
      audioTransport_->release();
      audioTransport_= NULL;
    }
    delete rtcpTracker_;
  }

  bool WebRtcConnection::init() {
    return true;
  }

  void WebRtcConnection::close() {
    ELOG_DEBUG("WebRtcConnection close");
    {
      boost::mutex::scoped_lock lock(queueMutex_);
      sending_ = false;
      cond_.notify_one();
    }

    // TODO: we're really not safe closing transports until sendLoop exits, unless sendloop
    // has it's own reference on transports

    if (videoTransport_) {
      videoTransport_->close();
    }
    if (audioTransport_) {
      audioTransport_->close();
    }
    // The muxer closes us so we should not receive any more data to deliver
  }
  
  bool WebRtcConnection::setRemoteSdp(const std::string &sdp) {
    ELOG_DEBUG("Set Remote SDP %s", sdp.c_str());
    remoteSdp_.initWithSdp(sdp);
    video_ = (remoteSdp_.videoSsrc==0?false:true);
    audio_ = (remoteSdp_.audioSsrc==0?false:true);

    CryptoInfo cryptLocal_video;
    CryptoInfo cryptLocal_audio;
    CryptoInfo cryptRemote_video;
    CryptoInfo cryptRemote_audio;

    bundle_ = remoteSdp_.isBundle;
    ELOG_DEBUG("Is bundle? %d %d ", bundle_, true);
    std::vector<RtpMap> payloadRemote = remoteSdp_.getPayloadInfos();
    localSdp_.getPayloadInfos() = remoteSdp_.getPayloadInfos();
    localSdp_.isBundle = bundle_;
    localSdp_.isRtcpMux = remoteSdp_.isRtcpMux;

    ELOG_DEBUG("Video %d videossrc %u Audio %d audio ssrc %u Bundle %d", video_, remoteSdp_.videoSsrc, audio_, remoteSdp_.audioSsrc,  bundle_);

    ELOG_DEBUG("Setting SSRC to localSdp %u", this->getVideoSinkSSRC());
    localSdp_.videoSsrc = this->getVideoSinkSSRC();
    localSdp_.audioSsrc = this->getAudioSinkSSRC();

    this->setVideoSourceSSRC(remoteSdp_.videoSsrc);
    this->setAudioSourceSSRC(remoteSdp_.audioSsrc);

    // Figure out which end we're tracking (if no remote streams were offered this is probably is a subscriber)
    bool outbound = video_ || audio_;
    SdpInfo &track_sdp = outbound ? remoteSdp_ : localSdp_;
    rtcpTracker_ = new RTCPTracker(outbound, remoteSdp_.getIceUsername(), track_sdp.videoSsrc);

    if (outbound) {
      if (remoteSdp_.videoBw*1000 >= config.highBWThreshold) {
        filterRtcp_ = config.filterHighBWRtcp;
        minBitrate_ = config.minHighBWBitRate;
        ELOG_DEBUG("High BW connection");
      } else {
        filterRtcp_ = config.filterAllRtcp;
        minBitrate_ = config.minBitRate;
      }
      ELOG_DEBUG("Filter rtcp: %s %u", filterRtcp_ ? "on" : "off", minBitrate_);
    }

    if (remoteSdp_.profile == SAVPF) {
      if (remoteSdp_.isFingerprint) {
        // DTLS-SRTP
        if (remoteSdp_.hasVideo) {
          videoTransport_ = new DtlsTransport(VIDEO_TYPE, "video", bundle_, remoteSdp_.isRtcpMux, this, stunServer_, stunPort_, minPort_, maxPort_);
        }
        if (remoteSdp_.hasAudio) {
          audioTransport_ = new DtlsTransport(AUDIO_TYPE, "audio", bundle_, remoteSdp_.isRtcpMux, this, stunServer_, stunPort_, minPort_, maxPort_);
        }
      } else {
        // SDES
        std::vector<CryptoInfo> crypto_remote = remoteSdp_.getCryptoInfos();
        for (unsigned int it = 0; it < crypto_remote.size(); it++) {
          CryptoInfo cryptemp = crypto_remote[it];
          if (cryptemp.mediaType == VIDEO_TYPE
              && !cryptemp.cipherSuite.compare("AES_CM_128_HMAC_SHA1_80")) {
            videoTransport_ = new SdesTransport(VIDEO_TYPE, "video", bundle_, remoteSdp_.isRtcpMux, &cryptemp, this, stunServer_, stunPort_, minPort_, maxPort_);
          } else if (!bundle_ && cryptemp.mediaType == AUDIO_TYPE
              && !cryptemp.cipherSuite.compare("AES_CM_128_HMAC_SHA1_80")) {
            audioTransport_ = new SdesTransport(AUDIO_TYPE, "audio", bundle_, remoteSdp_.isRtcpMux, &cryptemp, this, stunServer_, stunPort_, minPort_, maxPort_);
          }
        }
      }
    } else {
      // Handle non-encrypted RTP
      if (remoteSdp_.hasVideo) {
        videoTransport_ = new RTPTransport(VIDEO_TYPE, "video", bundle_, remoteSdp_.isRtcpMux, this, remoteSdp_.connectionHost, remoteSdp_.videoPort, localAddress_, minPort_, maxPort_);
      }
      if (remoteSdp_.hasAudio) {
        audioTransport_ = new RTPTransport(AUDIO_TYPE, "audio", bundle_, remoteSdp_.isRtcpMux, this, remoteSdp_.connectionHost, remoteSdp_.audioPort, localAddress_, minPort_, maxPort_);
      }
    }

    return true;
  }

  std::string WebRtcConnection::getLocalSdp() {
    ELOG_DEBUG("Getting SDP");
    if (videoTransport_ != NULL) {
      videoTransport_->processLocalSdp(&localSdp_);
    }
    ELOG_DEBUG("Video SDP done.");
    if (!bundle_ && audioTransport_ != NULL) {
      audioTransport_->processLocalSdp(&localSdp_);
    }
    ELOG_DEBUG("Audio SDP done.");
    localSdp_.profile = remoteSdp_.profile;
    return localSdp_.getSdp();
  }

  int WebRtcConnection::deliverAudioData(char* buf, int len) {
    writeSsrc(buf, len, this->getAudioSinkSSRC());
    if (bundle_){
      if (videoTransport_ != NULL) {
        if (audioEnabled_ == true) {
          this->queueData(0, buf, len, videoTransport_);
        }
      }
    } else if (audioTransport_ != NULL) {
      if (audioEnabled_ == true) {
        this->queueData(0, buf, len, audioTransport_);
      }
    }
    return len;
  }

  int WebRtcConnection::deliverVideoData(char* buf, int len) {
    rtpheader *head = (rtpheader*) buf;
    writeSsrc(buf, len, this->getVideoSinkSSRC());
    if (videoTransport_ != NULL) {
      if (videoEnabled_ == true /*&& rand() < RAND_MAX*0.6*/) {

        if (head->payloadtype == RED_90000_PT) {
          int totalLength = 12;

          if (head->extension) {
            totalLength += ntohs(head->extensionlength)*4 + 4; // RTP Extension header
          }
          int rtpHeaderLength = totalLength;
          redheader *redhead = (redheader*) (buf + totalLength);

          //redhead->payloadtype = remoteSdp_.inOutPTMap[redhead->payloadtype];
          if (!remoteSdp_.supportPayloadType(head->payloadtype)) {
            while (redhead->follow) {
              totalLength += redhead->getLength() + 4; // RED header
              redhead = (redheader*) (buf + totalLength);
            }
            // Parse RED packet to VP8 packet.
            // Copy RTP header
            memcpy(deliverMediaBuffer_, buf, rtpHeaderLength);
            // Copy payload data
            memcpy(deliverMediaBuffer_ + totalLength, buf + totalLength + 1, len - totalLength - 1);
            // Copy payload type
            rtpheader *mediahead = (rtpheader*) deliverMediaBuffer_;
            mediahead->payloadtype = redhead->payloadtype;
            buf = deliverMediaBuffer_;
            len = len - 1 - totalLength + rtpHeaderLength;
          }
        }
    
        this->queueData(0, buf, len, videoTransport_);
      } else {
        ELOG_DEBUG("Dropped video packet.");
      }
    }
    return len;
  }

  void WebRtcConnection::resetVideoStream() {
      boost::mutex::scoped_lock lock(queueMutex_);
	  resetVideo_ = true;
  }

  void WebRtcConnection::resetAudioStream() {
      boost::mutex::scoped_lock lock(queueMutex_);
	  resetAudio_ = true;
  }
  
  int WebRtcConnection::deliverFeedback(char* buf, int len){
    // Check where to send the feedback
    rtcpheader *chead = (rtcpheader*) buf;
    // RTP Feedback
    // ELOG_DEBUG("Received Feedback type %u ssrc %u, sourcessrc %u", chead->packettype, ntohl(chead->ssrc), ntohl(chead->ssrcsource));
    if (ntohl(chead->ssrcsource) == this->getAudioSourceSSRC()) {
        writeSsrc(buf,len,this->getAudioSinkSSRC());      
    } else {
        writeSsrc(buf,len,this->getVideoSinkSSRC());      
    }

    if (bundle_){
      if (videoTransport_ != NULL) {
        this->queueData(0, buf, len, videoTransport_);
      }
    } else {
      // TODO: Check where to send the feedback
      if (videoTransport_ != NULL) {
        this->queueData(0, buf, len, videoTransport_);
      }
    }
    return len;
  }

  void WebRtcConnection::writeSsrc(char* buf, int len, unsigned int ssrc) {
    rtpheader *head = (rtpheader*) buf;
    rtcpheader *chead = reinterpret_cast<rtcpheader*> (buf);
    //if it is RTCP we check it it is a compound packet
    if (chead->packettype == RTCP_Sender_PT || chead->packettype == RTCP_Receiver_PT || chead->packettype == RTCP_PS_Feedback_PT || chead->packettype == RTCP_RTP_Feedback_PT) {
        processRtcpHeaders(buf,len,ssrc);
    } else {
      head->ssrc=htonl(ssrc);
    }
  }

  void WebRtcConnection::writePayloadType(char* buf, int len) {
    rtpheader *head = (rtpheader*) buf;
    rtcpheader *chead = reinterpret_cast<rtcpheader*> (buf);
    if (chead->packettype == RTCP_Sender_PT || chead->packettype == RTCP_Receiver_PT || chead->packettype == RTCP_PS_Feedback_PT || chead->packettype == RTCP_RTP_Feedback_PT) {
    } else {
      head->payloadtype= remoteSdp_.inOutPTMap[head->payloadtype];
    }
  }

  void WebRtcConnection::onTransportData(char* buf, int length, Transport *transport) {
    if (audioSink_ == NULL && videoSink_ == NULL && fbSink_==NULL)
      return;
    if (length > 0)
      receivedBytes_.fetch_add(length);
    rtcpheader *chead = reinterpret_cast<rtcpheader*> (buf);
    if (chead->packettype == RTCP_Receiver_PT || chead->packettype == RTCP_PS_Feedback_PT || chead->packettype == RTCP_RTP_Feedback_PT){
      if (fbSink_ != NULL) {
        rtcpTracker_->checkRTCP(buf, length);
        fbSink_->deliverFeedback(buf,length);
      }
    } else {
      // RTP or RTCP Sender Report
      if (bundle_) {

        // Check incoming SSRC
        rtpheader *head = reinterpret_cast<rtpheader*> (buf);
        unsigned int recvSSRC = ntohl(head->ssrc);

        if (chead->packettype == RTCP_Sender_PT) { //Sender Report
          // RTP Feedback
          // ELOG_DEBUG ("RTP Sender Report %d length %d ", chead->packettype, ntohs(chead->length));
          recvSSRC = ntohl(chead->ssrc);
          if (recvSSRC == this->getVideoSourceSSRC()) {
            processSRHeader(buf);
          }
          rtcpTracker_->checkRTCP(buf, length);
        }

        // Deliver data
        if (recvSSRC==this->getVideoSourceSSRC() || recvSSRC==this->getVideoSinkSSRC()) {
          videoSink_->deliverVideoData(buf, length);
        } else if (recvSSRC==this->getAudioSourceSSRC() || recvSSRC==this->getAudioSinkSSRC()) {
          audioSink_->deliverAudioData(buf, length);
        } else {
          ELOG_DEBUG("Unknown SSRC %u, localVideo %u, remoteVideo %u, ignoring", recvSSRC, this->getVideoSourceSSRC(), this->getVideoSinkSSRC());
        }
      } else if (transport->mediaType == AUDIO_TYPE) {
        if (audioSink_ != NULL) {
          rtpheader *head = (rtpheader*) buf;
          // Firefox does not send SSRC in SDP
          if (this->getAudioSourceSSRC() == 0) {
            ELOG_DEBUG("Audio Source SSRC is %u", ntohl(head->ssrc));
            this->setAudioSourceSSRC(ntohl(head->ssrc));
            //this->updateState(TRANSPORT_READY, transport);
          }
          head->ssrc = htonl(this->getAudioSinkSSRC());
          audioSink_->deliverAudioData(buf, length);
        }
      } else if (transport->mediaType == VIDEO_TYPE) {
        if (videoSink_ != NULL) {
          rtpheader *head = (rtpheader*) buf;
          // Firefox does not send SSRC in SDP
          if (this->getVideoSourceSSRC() == 0) {
            ELOG_DEBUG("Video Source SSRC is %u", ntohl(head->ssrc));
            this->setVideoSourceSSRC(ntohl(head->ssrc));
            //this->updateState(TRANSPORT_READY, transport);
          }

          head->ssrc = htonl(this->getVideoSinkSSRC());
          videoSink_->deliverVideoData(buf, length);
        }
      }
    }
  }

  int WebRtcConnection::sendFirPacket() {
    ELOG_DEBUG("Generating FIR Packet");

    boost::mutex::scoped_lock lock(queueMutex_);
    if (sending_ == false)
      return 0;

    sequenceNumberFIR_++; // do not increase if repetition
    int pos = 0;
    uint8_t rtcpPacket[50];
    // add full intra request indicator
    uint8_t FMT = 4;
    rtcpPacket[pos++] = (uint8_t) 0x80 + FMT;
    rtcpPacket[pos++] = (uint8_t) 206;

    //Length of 4
    rtcpPacket[pos++] = (uint8_t) 0;
    rtcpPacket[pos++] = (uint8_t) (4);

    // Add our own SSRC
    uint32_t* ptr = reinterpret_cast<uint32_t*>(rtcpPacket + pos);
    ptr[0] = htonl(this->getVideoSinkSSRC());
    pos += 4;

    rtcpPacket[pos++] = (uint8_t) 0;
    rtcpPacket[pos++] = (uint8_t) 0;
    rtcpPacket[pos++] = (uint8_t) 0;
    rtcpPacket[pos++] = (uint8_t) 0;
    // Additional Feedback Control Information (FCI)
    uint32_t* ptr2 = reinterpret_cast<uint32_t*>(rtcpPacket + pos);
    ptr2[0] = htonl(this->getVideoSourceSSRC());
    pos += 4;

    rtcpPacket[pos++] = (uint8_t) (sequenceNumberFIR_);
    rtcpPacket[pos++] = (uint8_t) 0;
    rtcpPacket[pos++] = (uint8_t) 0;
    rtcpPacket[pos++] = (uint8_t) 0;

    if (videoTransport_ != NULL) {
      lock.unlock();
      videoTransport_->write((char*)rtcpPacket, pos);
    }

    return pos;
  }

  void WebRtcConnection::setWebRTCConnectionStateListener(
      WebRtcConnectionStateListener* listener) {
    this->connStateListener_ = listener;
  }

  void WebRtcConnection::updateState(TransportState state, Transport * transport) {
    WebRTCState temp = globalState_;
    ELOG_INFO("Update Transport State %s to %d", transport->transport_name.c_str(), state);
    if (audioTransport_ == NULL && videoTransport_ == NULL) {
      return;
    }

    if (state == TRANSPORT_FAILED) {
      temp = FAILED;
      ELOG_INFO("WebRtcConnection failed.");
    }

    
    if (globalState_ == FAILED) {
      // if current state is failed we don't use
      return;
    }

    if (state == TRANSPORT_STARTED &&
        (!remoteSdp_.hasAudio || (audioTransport_ != NULL && audioTransport_->getTransportState() == TRANSPORT_STARTED)) &&
        (!remoteSdp_.hasVideo || (videoTransport_ != NULL && videoTransport_->getTransportState() == TRANSPORT_STARTED))) {
      if (remoteSdp_.hasVideo) {
        videoTransport_->setRemoteCandidates(remoteSdp_.getCandidateInfos());
      }
      if (!bundle_ && remoteSdp_.hasAudio) {
        audioTransport_->setRemoteCandidates(remoteSdp_.getCandidateInfos());
      }
      temp = STARTED;
    }

    if (state == TRANSPORT_READY &&
        (!remoteSdp_.hasAudio || (audioTransport_ != NULL && audioTransport_->getTransportState() == TRANSPORT_READY)) &&
        (!remoteSdp_.hasVideo || (videoTransport_ != NULL && videoTransport_->getTransportState() == TRANSPORT_READY))) {
        // WebRTCConnection will be ready only when all channels are ready.
        temp = READY;
    }

    if (transport != NULL && transport == videoTransport_ && bundle_) {
      if (state == TRANSPORT_STARTED) {
        videoTransport_->setRemoteCandidates(remoteSdp_.getCandidateInfos());
        temp = STARTED;
      }
      if (state == TRANSPORT_READY) {
        temp = READY;
      }
    }

    if (temp == READY && globalState_ != temp) {
      ELOG_INFO("Ready to send and receive media");
    }

    if (audioTransport_ != NULL && videoTransport_ != NULL) {
      ELOG_INFO("%s - Update Transport State end, %d - %d, %d - %d, %d - %d", 
        transport->transport_name.c_str(),
        (int)audioTransport_->getTransportState(), 
        (int)videoTransport_->getTransportState(), 
        this->getAudioSourceSSRC(),
        this->getVideoSourceSSRC(),
        (int)temp, 
        (int)globalState_);
    }
    
    if (temp < 0) {
      return;
    }

    if (temp == globalState_ || (temp == STARTED && globalState_ == READY))
      return;

    globalState_ = temp;
    if (connStateListener_ != NULL)
      connStateListener_->connectionStateChanged(globalState_);
  }

  void WebRtcConnection::queueData(int comp, const char* buf, int length, Transport *transport) {
    boost::mutex::scoped_lock lock(queueMutex_);
    if (!sending_)
      return;
    if (audioSink_ == NULL && videoSink_ == NULL && fbSink_==NULL) //we don't enqueue data if there is nothing to receive it
      return;
    if (length > 0)
      sentBytes_.fetch_add(length);
    if (sendQueue_.size() < 1000 && length <= maxPacketSize) {
      dataPacket p_;
      //memset(p_.data, 0, length);
      memcpy(p_.data, buf, length);
      p_.comp = comp;
      if (transport->mediaType == VIDEO_TYPE) {
        p_.type = VIDEO_PACKET;
      } else {
        // Todo: presumably it would make sense to update payload type for video packets too
        writePayloadType(p_.data, length);
        p_.type = AUDIO_PACKET;
      }

      p_.length = length;
      sendQueue_.push(p_);
    } else {
        ELOG_ERROR("Discarded packed of size %d", length);
    }
    cond_.notify_one();
  }

  WebRTCState WebRtcConnection::getCurrentState() {
    return globalState_;
  }

  void WebRtcConnection::processRtcpHeaders(char* buf, int len, unsigned int ssrc){
    char* movingBuf = buf;
    int rtcpLength = 0;
    int totalLength = 0;
    do{
      movingBuf+=rtcpLength;
      rtcpheader *chead= reinterpret_cast<rtcpheader*>(movingBuf);
      rtcpLength= (ntohs(chead->length)+1)*4;      
      totalLength+= rtcpLength;
      if (chead->packettype == RTCP_PS_Feedback_PT){
        firheader *thefir = reinterpret_cast<firheader*>(movingBuf);
        if (thefir->fmt == 4){ // It is a FIR Packet, we generate it so sequence num is incremented
          this->sendFirPacket();
        } else if (thefir->fmt == 15){ // FB App Item
          processRembHeader(movingBuf, ssrc);
        }
      } else if (chead->packettype == RTCP_Receiver_PT){ // Receiver report
        processRRHeader(movingBuf, ssrc);
      }
      chead->ssrc=htonl(ssrc);
    } while(totalLength<len);
  }

  unsigned int WebRtcConnection::findLowestBitrate() {
    std::map<unsigned int, BitrateReport>::iterator iter;
    long long now = current_timestamp();
    unsigned int bitrate = 2000000;
    for (iter = receiverReports_.begin(); iter != receiverReports_.end(); iter++) {
      if (now - (*iter).second.last_received < 20000 && (*iter).second.bitrate < bitrate)
        bitrate = (*iter).second.bitrate;
    }
    return bitrate;
  }

  void WebRtcConnection::processRembHeader(void* buf, unsigned int ssrc) {
    rembheader *chead = reinterpret_cast<rembheader*>(buf);
    if (chead->uniqueid1 == 'R' && chead->uniqueid2 == 'E' && chead->uniqueid3 == 'M' && chead->uniqueid4 == 'B') {
      if (filterRtcp_) {
        // Store as latest report for this receiver
        BitrateReport rep(current_timestamp(), chead->getBitrate());
        receiverReports_[chead->ssrc] = rep;
        // Report back current minimum but cap at minimum
        unsigned int minBr = (findLowestBitrate() * 95) / 100;
        ELOG_DEBUG("Lowest br: %u", minBr);
        if (minBitrate_ > 0 && minBr < minBitrate_) {
          ELOG_DEBUG("Capping detected bitrate: %u", chead->getBitrate());
          minBr = minBitrate_;
        }
        chead->setBitrate(minBr);
      }
      ELOG_DEBUG("REMB %u bitrate: %u", chead->ssrc, chead->getBitrate());
    }
  }

  void WebRtcConnection::processRRHeader(void* buf, unsigned int ssrc) {
    rrheader *rhead = reinterpret_cast<rrheader*>(buf);
    reportblock *rb = rhead->reports;
    if (filterRtcp_) {
      // Force Chrome to use REMB estimates instead by pretending there's no loss
      rb->fractionlost = 0;
      // Chrome isn't currently using cumulative packet lost count in the send side bitrate estimate
      // so pass along as is.
      //rb->setPacketsLost(0);
    }
    ELOG_DEBUG("RR %u lost fraction: %u count: %u", rhead->ssrc, (unsigned)rb->fractionlost, rb->getPacketsLost());
  }

  void WebRtcConnection::processSRHeader(void* buf) {
    srheader *rhead = reinterpret_cast<srheader*>(buf);
    lastSrSentTs_ = rhead->getTs();
    lastSrSentPackets_ = rhead->getPackets();
    lastSrRecTs_ = current_timestamp();
    ELOG_DEBUG("SR %u ts: %u count: %u reports: %u", rhead->ssrc, lastSrSentTs_, lastSrSentPackets_, (unsigned)rhead->blockcount);
  }

  void WebRtcConnection::sendLoop() {
    ELOG_INFO("WebRtcConnection send thread started");

    while (true) {
      boost::unique_lock<boost::mutex> queuelock(queueMutex_);
      if (!sending_)
          break;
      if (sendQueue_.size() == 0) {
        cond_.wait(queuelock);
      } else {
        dataPacket packet(sendQueue_.front());
        sendQueue_.pop();

        if (packet.type == VIDEO_PACKET || bundle_) {
          if (resetVideo_) {
            queuelock.unlock();
            videoTransport_->reset();
            queuelock.lock();
            resetVideo_ = false;
          }
          queuelock.unlock();
          videoTransport_->write(packet.data, packet.length);
          rtcpTracker_->checkRTP(packet.data, packet.length);
        } else {
          if (resetAudio_) {
            queuelock.unlock();
            audioTransport_->reset();
            queuelock.lock();
            resetAudio_ = false;
          }
          queuelock.unlock();
          audioTransport_->write(packet.data, packet.length);
        }
      }
    }

    ELOG_INFO("WebRtcConnection send thread stopped");
    release();
  }

  void WebRtcConnection::getStats(unsigned int &sent, unsigned int &received) {
    received = receivedBytes_;
    sent = sentBytes_;
  }

}
/* namespace mcu */
