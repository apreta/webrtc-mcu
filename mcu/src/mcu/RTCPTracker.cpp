
#include "media/rtcp/rtcp_utility.h"

#include "RTCPTracker.h"
#include "rtputils.h"

namespace mcu {
  DEFINE_LOGGER(RTCPTracker, "RTCPTracker");

  using namespace webrtc::RTCPUtility;

  RTCPTracker::RTCPTracker(bool out, const std::string& id, uint32_t ssrc) :
        outbound_(out), id_(id), ssrc_(ssrc), send_seq_(0), rtp_reports_(0), sent_bytes_(0) {
    std::stringstream str;
    str << id_ << ":" << ssrc_;
    logId_ = str.str();
    ELOG_DEBUG("Initialized tracker: %s %s", out ? "outbound":"inbound", logId_.c_str());
  }

  RTCPPacketTypes processNack(log4cxx::LoggerPtr logger, const char * id, RTCPParserV2 &rtcp) {
    uint32_t from = rtcp.Packet().NACK.SenderSSRC;
    RTCPPacketTypes type = rtcp.Iterate();
    while (type == kRtcpRtpfbNackItemCode) {
      const RTCPPacket &packet(rtcp.Packet());
      ELOG_DEBUG("%s %u NACK: %u %x", id, from, packet.NACKItem.PacketID, packet.NACKItem.BitMask);
      type = rtcp.Iterate();
    }
    return type;
  }

  RTCPPacketTypes processSR(log4cxx::LoggerPtr logger, const char * id, RTCPParserV2 &rtcp) {
    uint32_t from;
    const RTCPPacket &packet(rtcp.Packet());
    if (rtcp.PacketType() == kRtcpSrCode) {
      from = packet.SR.SenderSSRC;
      ELOG_DEBUG("%s %u SR: ts %u count %u", id, from, packet.SR.RTPTimestamp, packet.SR.SenderPacketCount);
    } else {
      from = packet.RR.SenderSSRC;
      ELOG_DEBUG("%s %u RR", id, packet.RR.SenderSSRC);
    }
    RTCPPacketTypes type = rtcp.Iterate();
    while (type == kRtcpReportBlockItemCode) {
      const RTCPPacket &packet(rtcp.Packet());
      // report block
      ELOG_DEBUG("%s %u RB: %u lost %u fraction %u delay %u", id, from, packet.ReportBlockItem.SSRC, packet.ReportBlockItem.CumulativeNumOfPacketsLost, (unsigned)packet.ReportBlockItem.FractionLost, packet.ReportBlockItem.DelayLastSR);
      type = rtcp.Iterate();
    }
    return type;
  }

  RTCPPacketTypes processTMMBR(log4cxx::LoggerPtr logger, const char * id, RTCPParserV2 &rtcp) {
    uint32_t from;
    const RTCPPacket &packet(rtcp.Packet());
    from = packet.TMMBR.SenderSSRC;
    ELOG_DEBUG("%s %u TMMBR", id, from);
    RTCPPacketTypes type = rtcp.Iterate();
    while (type == kRtcpRtpfbTmmbrItemCode) {
      const RTCPPacket &packet(rtcp.Packet());
      // report block
      ELOG_DEBUG("%s %u TMMBR: %u bw %u oh %u", id, from, packet.TMMBRItem.SSRC, packet.TMMBRItem.MaxTotalMediaBitRate, packet.TMMBRItem.MeasuredOverhead);
      type = rtcp.Iterate();
    }
    return type;
  }

  RTCPPacketTypes processTMMBN(log4cxx::LoggerPtr logger, const char * id, RTCPParserV2 &rtcp) {
    uint32_t from;
    const RTCPPacket &packet(rtcp.Packet());
    from = packet.TMMBN.SenderSSRC;
    ELOG_DEBUG("%s %u TMMBN", id, from);
    RTCPPacketTypes type = rtcp.Iterate();
    while (type == kRtcpRtpfbTmmbnItemCode) {
      const RTCPPacket &packet(rtcp.Packet());
      // report block
      ELOG_DEBUG("%s %u TMMBN: %u bw %u oh %u", id, from, packet.TMMBNItem.SSRC, packet.TMMBNItem.MaxTotalMediaBitRate, packet.TMMBNItem.MeasuredOverhead);
      type = rtcp.Iterate();
    }
    return type;
  }
  
  RTCPPacketTypes processRemb(log4cxx::LoggerPtr logger, const char * id, RTCPParserV2 &rtcp) {
    uint32_t from = rtcp.Packet().PSFBAPP.SenderSSRC;
    RTCPPacketTypes type = rtcp.Iterate();
    while (type == kRtcpPsfbRembItemCode) {
      const RTCPPacket &packet(rtcp.Packet());
      // report block (we only have one video stream with remb enabled so only one ssrc should be reported)
      ELOG_DEBUG("%s %u REMB: bw %u source %u", id, from, packet.REMBItem.BitRate, packet.REMBItem.SSRCs[0]);
      type = rtcp.Iterate();
    }
    return type;
  }

  void RTCPTracker::checkRTP(char* buf, int len) {
    rtpheader *head = (rtpheader*) buf;
    uint16_t seq = ntohs(head->seqnum);
    
    if (head->payloadtype == RED_90000_PT || head->payloadtype == VP8_90000_PT) {
      ++rtp_reports_;
      sent_bytes_ += len;

      if (seq < send_seq_+1) {
        if (send_seq_ > 0xF000 && seq < 0x1000) {
          send_seq_ = 0;
        } else {
          ELOG_DEBUG("%s Resend %u %u", logId_.c_str(), seq, head->payloadtype);
        }
      } else {
        send_seq_ = seq;
        if (rtp_reports_ % 50 == 0) {
          ELOG_DEBUG("%s Send %u %d %u", logId_.c_str(), seq, sent_bytes_, head->payloadtype);
        }
      }
    }
  }

  void RTCPTracker::checkRTCP(char* buf, int len) {

    RTCPParserV2 rtcp((uint8_t *)buf, len, true);
    const char *id = logId_.c_str();

    if (rtcp.IsValid()) {
      RTCPPacketTypes type = rtcp.Begin();
      while (type != kRtcpNotValidCode) {
        switch (type) {
          case kRtcpRtpfbNackCode:
            type = processNack(logger, id, rtcp);
            break;

          case kRtcpPsfbPliCode:
            ELOG_DEBUG("%s %u PLI", id, rtcp.Packet().PLI.SenderSSRC);
            type = rtcp.Iterate();
            break;

          case kRtcpSrCode:
          case kRtcpRrCode:
            type = processSR(logger, id, rtcp);
            break;

          case kRtcpPsfbRembCode:
            type = processRemb(logger, id, rtcp);
            break;

          case kRtcpRtpfbTmmbrCode:
            type = processTMMBR(logger, id, rtcp);
            break;

          case kRtcpRtpfbTmmbnCode:
            type = processTMMBN(logger, id, rtcp);
            break;

          default:
            type = rtcp.Iterate();
        }
      }
    } else {
      ELOG_DEBUG("Invalid RTCP packet");
    }
  }


}