#ifndef REFCOUNT
#define REFCOUNT

#include <boost/atomic.hpp>

namespace mcu {

  class RefCounted {
    public:
      virtual void add_ref() = 0;
      virtual int release() = 0;
  };

  class RefCounter : public virtual RefCounted {
    public:
      RefCounter() : refcount_(1) {}
      virtual ~RefCounter() { }

      virtual void add_ref() {
        refcount_.fetch_add(1, boost::memory_order_relaxed);
      }

      virtual int release() {
        if (refcount_.fetch_sub(1, boost::memory_order_release) == 1) {
            boost::atomic_thread_fence(boost::memory_order_acquire);
            delete this;
            return 0;
        }
        return 1;
      }

    private:
      mutable boost::atomic<int> refcount_;
  };

}
#endif

