/*
 * RTPConnection.cpp
 */

#include <cstdio>
#include <gio/gio.h>

#include "RTPConnection.h"
#include "SdpInfo.h"

namespace mcu {
  DEFINE_LOGGER(RTPConnection, "RTPConnection");


  RTPConnection::RTPConnection(MediaType med,
      const std::string &transport_name, const std::string& remoteServer, uint remotePort, const std::string& local, int minPort, int maxPort) :
      mediaType(med), localAddress(local), minPort_(minPort), maxPort_(maxPort) {
    listener_ = NULL;
    cancellable_ = NULL;
    transportName.reset(new std::string(transport_name));

    ELOG_DEBUG("Setting remote address %s %u", remoteServer.c_str(), remotePort);
    GInetAddress *iaddr = g_inet_address_new_from_string(remoteServer.c_str());
    remoteAddress_ = g_inet_socket_address_new(iaddr, remotePort);
  }

  RTPConnection::~RTPConnection() {
    ELOG_DEBUG("RTPConnection Destructor");
    m_Thread_.interrupt();
    m_Thread_.detach();
    /*if (!m_Thread_.try_join_for(boost::chrono::seconds(1))) {
      ELOG_ERROR("RTPConnection thread didn't exit, abandoning");
      m_Thread_.detach();
      return;
    } */
    g_object_unref(remoteAddress_);
    g_object_unref(socket_);
  }

  void RTPConnection::close() {
    ELOG_DEBUG("RTPConnection close");
    if (rtpState != RTP_FINISHED) {
      if (cancellable_ != NULL)
        g_cancellable_cancel(cancellable_);
      updateRTPState(RTP_FINISHED);
    }
  }

  void RTPConnection::start() {
    add_ref();
    cancellable_ = g_cancellable_new();
    m_Thread_ = boost::thread(&RTPConnection::init, this);
  }

  int RTPConnection::sendData(unsigned int compId, const void* buf, int len) {
    int val = -1;
    if (rtpState != RTP_READY) {
            return val;
    }
    GError *err = NULL;
    val = g_socket_send_to(socket_, remoteAddress_, (const gchar*) buf, len, cancellable_, &err);
    if (val != len) {
      ELOG_DEBUG("Data sent %d of %d", val, len);
    }
    return val;
  }

  void RTPConnection::init() {
    GError *err = NULL;
    socket_ = g_socket_new(G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_DATAGRAM, G_SOCKET_PROTOCOL_UDP, &err);
    if (err != NULL) {
      ELOG_ERROR("Error creating UDP socket");
      return;
    }

    GInetAddress *iaddr = g_inet_address_new_from_string(localAddress.c_str());
    GSocketAddress *addr = g_inet_socket_address_new(iaddr, 0);
    if (!g_socket_bind(socket_, G_SOCKET_ADDRESS(addr), TRUE, &err)) {
      ELOG_ERROR("Error binding UDP socket");
    }
    GInetSocketAddress *bound_addr = (GInetSocketAddress*) g_socket_get_local_address(socket_, &err);
    localPort = g_inet_socket_address_get_port(bound_addr);
    ELOG_DEBUG("RTP UDP port allocated: %d", localPort);

    this->updateRTPState(RTP_INITIAL);

    // Node app is checking for state change every 100ms, so create artificial delay
    // so it sees this one
    usleep(1000 * 200);
      
    this->updateRTPState(RTP_READY);

    gchar *buffer = (gchar*) g_malloc(4096);

    // g_socket_condition_timed_wait () -- wait for G_IO_IN|G_IO_ERR|G_IO_HUP|G_IO_NVAL

    while (!g_cancellable_is_cancelled(cancellable_)) {
      int len = g_socket_receive_from(socket_, NULL, buffer, 4096, cancellable_, &err);
      if (len > 0) {
        getRTPListener()->onRTPData(buffer, len, this);
      }
      if (len < 0) {
        ELOG_ERROR("UDP socket error");
        break;
      }
    }

    g_socket_close(socket_, &err);
      
    ELOG_DEBUG("RTPConnection receiver stopped");
    listener_->onRTPClosed();

    listener_->release();
    listener_ = NULL;

    g_object_unref(addr);
    g_free(buffer);

    release();
  }

  void RTPConnection::setRTPListener(RTPConnectionListener *listener) {
    // Maybe we could use boost::intrusive_ptr...
    listener->add_ref();
    if (listener_)
      listener_->release();
    this->listener_ = listener;
  }

  RTPConnectionListener* RTPConnection::getRTPListener() {
    return this->listener_;
  }

  void RTPConnection::updateRTPState(RTPState state) {
    ELOG_DEBUG("%s - RTP State Changed %u", transportName->c_str(), state);
    this->rtpState = state;
    if (this->listener_ != NULL) {
      this->listener_->updateRTPState(state, this);
    }
  }

} /* namespace mcu */
