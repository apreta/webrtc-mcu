#!/bin/bash
runcmake() {
   # N.B.: mcuAPI (addon.node) will prefer make version in "build" to this
   # version in "xcode", so clean "build" and rebuild mcuAPI before running
   cmake -G Xcode ../src
   echo "Done"
}
if [ -d /usr/local/opt/boost155 ]; then
    # Probably on Mac, use older Boost to make Ubuntu 14.04
    export BOOST_ROOT=/usr/local/opt/boost155
fi
BIN_DIR="xcode"
if [ -d $BIN_DIR ]; then
  cd $BIN_DIR
  runcmake
else
  mkdir $BIN_DIR
  cd $BIN_DIR
  runcmake
fi

