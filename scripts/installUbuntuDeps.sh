#!/bin/bash
SCRIPT=`pwd`/$0
FILENAME=`basename $SCRIPT`
PATHNAME=`dirname $SCRIPT`
ROOT=$PATHNAME/..
BUILD_DIR=$ROOT/build
CURRENT_DIR=`pwd`

LIB_DIR=$BUILD_DIR/libdeps
PREFIX_DIR=$LIB_DIR/build/

pause() {
  read -p "$*"
}

parse_arguments(){
  while [ "$1" != "" ]; do
    case $1 in
      "--enable-gpl")
        ENABLE_GPL=true
        ;;
      "--single")
        shift
        SINGLE_BUILD="$1"
        ;;
    esac
    shift
  done
}

install_apt_deps(){
  # sudo apt-get install python-software-properties
  # sudo apt-get install software-properties-common
  # sudo add-apt-repository ppa:chris-lea/node.js
  curl -sL https://deb.nodesource.com/setup_0.10 | sudo -E bash -
  sudo apt-get install nodejs nodejs-legacy npm build-essential
  sudo apt-get install git make gcc g++ cmake libglib2.0-dev pkg-config liblog4cxx10-dev curl gtk-doc-tools
  #sudo apt-get install libboost1.55-dev libboost-thread1.55-dev libboost-regex1.55-dev 
  sudo apt-get install libboost-dev libboost-thread-dev libboost-regex-dev 
  # gtk-doc-tools is for libnice
  sudo npm install -g node-gyp
  sudo chown -R `whoami` ~/.npm
}

install_openssl(){
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    curl -O http://www.openssl.org/source/openssl-1.0.1s.tar.gz
    tar -zxvf openssl-1.0.1s.tar.gz
    cd openssl-1.0.1s
    ./config --prefix=$PREFIX_DIR -fPIC
    make -s V=0
    make install_sw
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_openssl
  fi
}

install_libnice(){
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    curl -L https://github.com/quicklyfrozen/libnice/archive/launchpad.tar.gz -o libnice-launchpad.tar.gz
    tar -zxvf libnice-launchpad.tar.gz
    cd libnice-launchpad
    ./autogen.sh
    CFLAGS="-g" ./configure --prefix=$PREFIX_DIR
    make -s V=0
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_libnice
  fi
}

# install_libboost(){
#  if [ -d $LIB_DIR ]; then
#	# not sure if needed ... sudo apt-get install libbz2-dev
#    cd $LIB_DIR
#    curl -LO http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.gz
#    tar -zxf boost_1_55_0.tar.gz
#    cd boost_1_55_0
#	./bootstrap.sh --prefix=$PREFIX_DIR
#	./b2 install --with-system --with-thread --with-chrono --with-regex --with-atomic link=shared threading=multi
#    cd $CURRENT_DIR
#  else
#    mkdir -p $LIB_DIR
#    install_libboost
#  fi
#}

install_libsrtp(){
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    curl -L https://github.com/quicklyfrozen/libsrtp/archive/master.tar.gz -o libsrtp-master.tar.gz
	tar -zxvf libsrtp-master.tar.gz
	cd libsrtp-master
    CFLAGS="-g -fPIC" ./configure --prefix=$PREFIX_DIR --enable-debug
    make -s V=0
    make uninstall
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_libsrtp
  fi
}

install_mediadeps(){
  sudo apt-get install yasm libvpx-dev libx264.
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    curl -O https://www.libav.org/releases/libav-9.20.tar.gz
    tar -zxvf libav-9.20.tar.gz
    cd libav-9.20
    ./configure --prefix=$PREFIX_DIR --enable-shared --enable-gpl --enable-libvpx --enable-libx264
    make -s V=0
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_mediadeps
  fi

}

install_mediadeps_nogpl(){
  sudo apt-get install yasm libvpx-dev
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    curl -O https://www.libav.org/releases/libav-9.20.tar.gz
    tar -zxvf libav-9.20.tar.gz
    cd libav-9.20
    ./configure --prefix=$PREFIX_DIR --enable-shared --enable-libvpx
    make -s V=0
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_mediadeps_nogpl
  fi
}

parse_arguments $*

mkdir -p $PREFIX_DIR

if [ -n "$SINGLE_BUILD" ]; then
    echo "Installing $SINGLE_BUILD"
    install_$SINGLE_BUILD
    exit 0
fi

echo "Installing deps via apt-get..."
install_apt_deps

echo "Installing openssl library..."
install_openssl

echo "Installing libnice library..."
install_libnice

echo "Installing libsrtp library..."
install_libsrtp

# echo "Installing boost library..."
# install_libboost

if [ "$ENABLE_GPL" = "true" ]; then
  echo "GPL libraries enabled"
  install_mediadeps
else
  echo "No GPL libraries enabled, this disables h264 transcoding, to enable gpl please use the --enable-gpl option"
  install_mediadeps_nogpl
fi
