#!/bin/bash

SCRIPT=`pwd`/$0
FILENAME=`basename $SCRIPT`
PATHNAME=`dirname $SCRIPT`
ROOT=$PATHNAME/..
BUILD_DIR=$ROOT/build
CURRENT_DIR=`pwd`
LIB_DIR=$BUILD_DIR/libdeps
PREFIX_DIR=$LIB_DIR/build/

SUDO="sudo"
OS=${OSTYPE//[0-9.]/}
#if [[ "$OS" == "darwin" ]]; then
#	SUDO=""
#fi

pause() {
  read -p "$*"
}

install_mcu(){
  cd $ROOT/mcu
  ./generateProject.sh
  ./buildProject.sh
  export MCU_HOME=`pwd`
  cd $CURRENT_DIR
}

install_mcu_api(){
  cd $ROOT/mcuAPI
  ./build.sh
  cd $CURRENT_DIR
}

install_mcu_controller(){
  cd $ROOT/mcu_controller
  ./install_mcu_controller.sh
  cd $CURRENT_DIR
}

echo 'Installing mcu...'
install_mcu
echo 'Installing mcuAPI...'
install_mcu_api
echo 'Installing mcuController...'
install_mcu_controller
