#!/bin/bash

echo [mcu_controller] Installing node_modules for mcu_controller

cd mcuController

npm install --loglevel error amqp socket.io winston
npm install sip modesl underscore

echo [mcu_controller] Done, node_modules installed

cd ../mcuClient/tools

./compile.sh
./compilefc.sh

echo [mcu_controller] Done, mcu.js compiled