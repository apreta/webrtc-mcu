/*global require, logger. setInterval, clearInterval, Buffer, exports*/
var crypto = require('crypto');
var rpc = require('./rpc/rpc');
var controller = require('./webRtcController');
var ST = require('./Stream');
var http = require('http');
var https = require('https');
var config = require('./../../mcu_config');
var logger = require('./logger').logger;
var Permission = require('./permission');
var sipController = require('./sipController');
var fs = require('fs');
var _ = require('underscore');

var server;

if (config.mcu.wscert) {
    var options = {
      key: fs.readFileSync(config.mcu.wskey),
      cert: fs.readFileSync(config.mcu.wscert)
    };
    if (config.mcu.wsca) options.ca = [fs.readFileSync(config.mcu.wsca)];
    server = https.createServer(options);
} else {
    server = http.createServer();
}

var io = require('socket.io').listen(server, {/*log:false*/});

try {
var segfault = require('segfault-handler');
    segfault.registerHandler();
} catch (e) {
    logger.error("Couldn't set up exception handler, continuing");
}

server.listen(config.mcu.wsport);

//io.set('log level', 0);

var rpcKey = config.rpc.superserviceKey;

var WARNING_N_ROOMS = config.mcuController.warning_n_rooms;
var LIMIT_N_ROOMS = config.mcuController.limit_n_rooms;

var INTERVAL_TIME_KEEPALIVE = config.mcuController.interval_time_keepAlive;

var myId;
var rooms = {};
var myState;


var findSocket = function(id) {
    return io.sockets.connected[id];
};

var calculateSignature = function (token, key) {
    "use strict";

    var toSign = token.tokenId + ',' + token.host,
        signed = crypto.createHmac('sha1', key).update(toSign).digest('hex');
    return (new Buffer(signed)).toString('base64');
};

var checkSignature = function (token, key) {
    "use strict";

    var calculatedSignature = calculateSignature(token, key);

    if (calculatedSignature !== token.signature) {
        logger.info('Auth fail. Invalid signature.');
        return false;
    } else {
        return true;
    }
};

/*
 * Sends a message of type 'type' to all sockets in a determined room.
 */
var sendMsgToRoom = function (room, type, arg) {
    "use strict";

    var sockets = room.sockets,
        id;
    for (id in sockets) {
        if (sockets.hasOwnProperty(id)) {
            logger.info('Sending message to', sockets[id], 'in room ', room.id);
            var s = findSocket(sockets[id]);
            if (s) s.emit(type, arg);
        }
    }

    sipController.notify(room, type, arg);
};


/*
 * Find socket for user by session id or guest id.
 */
var findUser = function (room, sessionId) {
    "use strict";

    if (rooms[room] != undefined) {
        var sockets = rooms[room].sockets;

        for (var id in sockets) {
            if (sockets.hasOwnProperty(id)) {
                var user = findSocket(sockets[id]).user;
                if (user && (user.session_id == sessionId || user.guest_id == sessionId))
                    return sockets[id];
            }
        }
    }
    return null;
};

/*
 * Process state change app messages and update local copy.
 */
var updateAppState = function(room, msg) {
    if (msg.ctl && (msg.ctl == 'delta' || msg.ctl == 'initial')) {
        if (!room.appState) room.appState = {};
        if (msg.updates) room.appState = _.extend(room.appState, msg.updates);
        if (msg.removes) room.appState = _.omit(room.appState, msg.removes);
    }
};


var privateRegexp;
var publicIP;

var addToCloudHandler = function (callback) {
    "use strict";

    var interfaces = require('os').networkInterfaces(),
        addresses = [],
        k,
        k2,
        address;


    for (k in interfaces) {
        if (interfaces.hasOwnProperty(k)) {
            for (k2 in interfaces[k]) {
                if (interfaces[k].hasOwnProperty(k2)) {
                    address = interfaces[k][k2];
                    if (address.family === 'IPv4' && !address.internal) {
                        addresses.push(address.address);
                    }
                }
            }
        }
    }

    publicIP = addresses[0];
    privateRegexp = new RegExp(publicIP, 'g');

    if (config.mcu.publicIP)
        publicIP = config.mcu.publicIP;

    var addECToCloudHandler = function(attempt) {
        if (attempt <= 0) {
            logger.error('Connection attempts exhausted, exiting');
            process.exit(1);
        }

        var retry = function() {
            setTimeout(function() {
                attempt = attempt - 1;
                addECToCloudHandler(attempt);
            }, 5000);
        };

        rpc.callRpc('rpc', 'addNewMcuController', {cloudProvider: config.cloudProvider.name, ip: publicIP}, function (msg) {

            if (msg === 'timeout') {
                logger.info('CloudHandler does not respond');
                if (rpc.connected()) retry();
                return;
            }
            if (msg == 'error') {
                logger.info('Error in communication with cloudProvider');
            }

            publicIP = msg.publicIP;
            myId = msg.id;
            myState = 2;

            var ping = function() {
                rpc.callRpc('rpc', 'keepAlive', myId, function (result) {
                    if (result === 'ok') {
                        setTimeout(ping, INTERVAL_TIME_KEEPALIVE);
                        attempt = 50;
                    } else if (result === 'whoareyou') {
                        logger.info('Not registered in cloud handler, retrying');
                        rpc.callRpc('rpc', 'killMe', publicIP, function () {});
                        if (rpc.connected()) retry();
                    } else {
                        logger.info('Keep alive failure: ' + result);
                        if (rpc.connected()) retry();
                    }
                });
            };
            setTimeout(ping, INTERVAL_TIME_KEEPALIVE);

            callback();

        });
    };

    addECToCloudHandler(50);
};

//*******************************************************************
//       When adding or removing rooms we use an algorithm to check the state
//       If there is a state change we send a message to cloudHandler
//
//       States:
//            0: Not available
//            1: Warning
//            2: Available
//*******************************************************************
var updateMyState = function (event, id) {
    "use strict";

    var nRooms = 0, newState, i, info;

    for (i in rooms) {
        if (rooms.hasOwnProperty(i)) {
            nRooms += 1;
        }
    }
    
    if (event !== undefined) {
        info = {event: event, room: id, rooms: nRooms};
        rpc.callRpc('rpc', 'roomEvent', info, function() {});
    }
    
    if (nRooms < WARNING_N_ROOMS) {
        newState = 2;
    } else if (nRooms > LIMIT_N_ROOMS) {
        newState = 0;
    } else {
        newState = 1;
    }

    if (newState === myState) {
        return;
    }

    myState = newState;

    info = {id: myId, state: myState};
    rpc.callRpc('rpc', 'setInfo', info, function () {});
};


/*
 * Main server for socket.io connections
 */
var listen = function () {
    "use strict";

    io.sockets.on('connection', function (socket) {

        logger.info("Socket connect ", socket.id);

        // Gets 'token' messages on the socket. Checks the signature and ask rpc if it is valid.
        // Then registers it in the room and callback to the client.
        socket.on('token', function (token, callback) {

            var tokenDB, user, streamList = [], userList = [], index, prior = null;

            if (checkSignature(token, rpcKey)) {

                rpc.callRpc('rpc', 'deleteToken', token.tokenId, function (resp) {

                    if (resp === 'error') {
                        logger.info('Token does not exist');
                        callback('error', 'Token does not exist');
                        socket.disconnect();

                    } else if (resp === 'timeout') {
                        logger.info('Manager does not respond');
                        callback('error', 'Manager does not respond');
                        socket.disconnect();

                    } else if (token.host === resp.host) {
                        tokenDB = resp;
                        if (rooms[tokenDB.room] === undefined) {
                            var room = {};
                            room.id = tokenDB.room;
                            room.sockets = [];
                            room.sockets.push(socket.id);
                            room.streams = {}; //streamId: Stream
                            if (tokenDB.p2p) {
                                logger.info('Token of p2p room');
                                room.p2p = true;
                            } else {
                                room.webRtcController = new controller.WebRtcController();
                            }
                            rooms[tokenDB.room] = room;
                            updateMyState('added', tokenDB.room);
                        } else {
                            if (rooms[tokenDB.room].deleteTimer) clearTimeout(rooms[tokenDB.room].deleteTimer);
                            prior = findUser(tokenDB.room, tokenDB.session);
                            if (!prior) prior = findUser(tokenDB.room, tokenDB.guestId);
                            rooms[tokenDB.room].sockets.push(socket.id);
                        }
                        user = {name: tokenDB.userName, role: tokenDB.role, session_id: tokenDB.session, guest_id: tokenDB.guestId};
                        socket.user = user;
                        var permissions = config.roles[tokenDB.role] || [];
                        socket.user.permissions = {};
                        for (var i in permissions) {
                            var permission = permissions[i];
                            socket.user.permissions[permission] = true;
                        }
                        socket.room = rooms[tokenDB.room];
                        socket.streams = []; //[list of streamIds]
                        socket.state = 'sleeping';

                        logger.info('OK, Valid token');

                        for (index in socket.room.streams) {
                            if (socket.room.streams.hasOwnProperty(index)) {
                                streamList.push(socket.room.streams[index].getPublicStream());
                            }
                        }

                        if (prior) {
                            logger.info("Disconnecting prior socket " + prior + " for user " + user.name);
                            var s = findSocket(prior);
                            if (s) {
                                s.emit('duplicate');
                                setTimeout(function() {
                                    s.disconnect();
                                }, 200);
                            }
                        }

                        for (index in socket.room.sockets) {
                            if (socket.room.sockets.hasOwnProperty(index)) {
                                userList.push(findSocket(socket.room.sockets[index]).user);
                            }
                        }

                        callback('success', {streams: streamList,
                                            users: userList,
                                            appState: socket.room.appState ? socket.room.appState : null, 
                                            id: socket.room.id, 
                                            p2p: socket.room.p2p,
                                            defaultVideoBW: config.mcuController.defaultVideoBW,
                                            maxVideoBW: config.mcuController.maxVideoBW,
                                            stunServerUrl: config.mcuController.stunServerUrl,
                                            turnServer: config.mcuController.turnServer
                                            });

                    } else {
                        logger.info('Invalid host');
                        callback('error', 'Invalid host');
                        socket.disconnect();
                    }
                });

            } else {
                callback('error', 'Authentication error');
                socket.disconnect();
            }
        });

        //Gets 'sendDataStream' messages on the socket in order to write a message in a dataStream.
        socket.on('sendDataStream', function (msg) {
            /*var sockets = socket.room.streams[msg.id].getDataSubscribers(), id;
            for (id in sockets) {
                if (sockets.hasOwnProperty(id)) {
                    logger.info('Sending dataStream to', sockets[id], 'in stream ', msg.id, 'message', msg.msg);
                    findSocket(sockets[id]).emit('onDataStream', msg);
                }
            }*/

            updateAppState(socket.room, msg.msg);
            
            // Dispatch data messages to all room members, not just subscribers
            var sockets = socket.room.sockets, id;
            for (id in sockets) {
                if (sockets.hasOwnProperty(id) && sockets[id] != socket) {
                    logger.info('Sending dataStream to', id, 'in stream ', msg.id, 'message', msg.msg);
                    findSocket(sockets[id]).emit('onDataStream', msg);
                }
            }
        });

        //Gets 'publish' messages on the socket in order to add new stream to the room.
        socket.on('publish', function (options, sdp, callback) {
            var id, st;
            if (!socket.user.permissions[Permission.PUBLISH]) {
                callback('error', 'unauthorized');
                return;
            }
            if (options.state === 'url') {
                id = Math.random() * 1000000000000000000;
                socket.room.webRtcController.addExternalInput(id, sdp, function (result) {
                    if (result === 'success') {
                        st = new ST.Stream({id: id, audio: options.audio, video: options.video, data: options.data, attributes: options.attributes});
                        socket.streams.push(id);
                        socket.room.streams[id] = st;
                        callback(result, id);
                        sendMsgToRoom(socket.room, 'onAddStream', st.getPublicStream());
                    } else {
                        callback(result);
                    }
                });
            } else if (options.state !== 'data' && !socket.room.p2p) {
                if (options.state === 'offer' && socket.state === 'sleeping') {
                    id = Math.floor(Math.random() * 1000000000000000000);
                    logger.info('Socket',socket.id,"publishing stream",id);
                    socket.room.webRtcController.addPublisher(id, sdp, function (answer) {
                        socket.state = 'waitingOk';
                        answer = answer.replace(privateRegexp, publicIP);
                        callback(answer, id);
                    }, function() {
                        if (socket.room.streams[id] !== undefined) {
                            sendMsgToRoom(socket.room, 'onAddStream', socket.room.streams[id].getPublicStream());
                        }
                    }, function(type, args) {
                        socket.emit(type, args);
                    });

                } else if (options.state === 'ok' && socket.state === 'waitingOk') {
                    st = new ST.Stream({id: options.streamId, audio: options.audio, video: options.video, data: options.data, screen: options.screen, attributes: options.attributes});
                    socket.state = 'sleeping';
                    socket.streams.push(options.streamId);
                    socket.room.streams[options.streamId] = st;
                }
            } else if (options.state === 'p2pSignaling') {
                findSocket(options.subsSocket).emit('onPublishP2P', {sdp: sdp, streamId: options.streamId}, function(answer) {
                    callback(answer);
                });
            } else {
                id = Math.floor(Math.random() * 1000000000000000000);
                st = new ST.Stream({id: id, socket: socket.id, audio: options.audio, video: options.video, data: options.data, screen: options.screen, attributes: options.attributes});
                socket.streams.push(id);
                socket.room.streams[id] = st;
                callback(undefined, id);
                sendMsgToRoom(socket.room, 'onAddStream', st.getPublicStream());

            }
        });

        //Updates attributes for a stream

        function mergeObj(newObj, oldObj) {
            for (var i in newObj) {
                if (newObj.hasOwnProperty(i)) {
                    if (typeof newObj[i] == 'object') {
                        if (typeof oldObj[i] != 'object')
                            oldObj[i] = {};
                        mergeObj(newObj[i], oldObj[i]);
                    } else {
                        oldObj[i] = newObj[i];
                    }
                }
            }
        }

        socket.on("updateAttributes", function(options){
          var empty = true, i;
          for(i in options.attributes) empty = false;

          if (!empty){
            var st = socket.room.streams[options.id]
            var attrs = st.getAttributes();

            mergeObj(options.attributes, attrs);

            if (st.updateAttributes(attrs)) {
               sendMsgToRoom(socket.room, 'onUpdateAttributes', st.getPublicStream());
            }
          }
        });

        //Gets 'subscribe' messages on the socket in order to add new subscriber to a determined stream (options.streamId).
        socket.on('subscribe', function (options, sdp, callback) {
            if (!socket.user.permissions[Permission.SUBSCRIBE]) {
                callback('error', 'unauthorized');
                return;
            }
            var stream = socket.room.streams[options.streamId];

            if (stream === undefined) {
                return;
            }

            if (stream.hasData() && options.data !== false) {
                stream.addDataSubscriber(socket.id);
            }

            if (sdp != undefined && (stream.hasAudio() || stream.hasVideo() || stream.hasScreen())) {

                if (socket.room.p2p) {
                    var s = stream.getSocket();
                    findSocket(s).emit('onSubscribeP2P', {streamId: options.streamId, subsSocket: socket.id}, function(offer) {
                        callback(offer);
                    });

                } else {
                    socket.room.webRtcController.addSubscriber(socket.id, options.streamId, options.audio, options.video, sdp, function (answer) {
                        answer = answer.replace(privateRegexp, publicIP);
                        callback(answer);
                    });
                }
            } else {
                callback(undefined);
            }

        });

        //Gets 'startRecorder' messages
        socket.on('startRecorder', function (options) {
          if (!socket.user.permissions[Permission.RECORD]) {
              callback('error', 'unauthorized');
              return;
          }
          var streamId = options.to;
          var url = options.url;
          logger.info("mcuController.js: Starting recorder streamID " + streamId + " url " + url);
            if (socket.room.streams[streamId].hasAudio() || socket.room.streams[streamId].hasVideo() || socket.room.streams[streamId].hasScreen()) {
                socket.room.webRtcController.addExternalOutput(streamId, url);
                logger.info("mcuController.js: Recorder Started");
            }
        });

        socket.on('stopRecorder', function (options) {
          if (!socket.user.permissions[Permission.RECORD]) {
              callback('error', 'unauthorized');
              return;
          }
          logger.info("mcuController.js: Stoping recorder to streamId " + options.to + " url " + options.url);
          socket.room.webRtcController.removeExternalOutput(options.to, options.url);
        });
        
        // Pause a video stream to a particular user
        socket.on('pauseVideo', function (options) {
			logger.info("mcuController.js: Pausing video stream " + options.id + " for " + socket.id);
			socket.room.webRtcController.pauseVideoStream(socket.id, options.id, options.enable);
		});

        // Pause an audio stream to a particular user
        socket.on('pauseAudio', function (options) {
			logger.info("mcuController.js: Pausing audio stream " + options.id + " for " + socket.id);
			socket.room.webRtcController.pauseAudioStream(socket.id, options.id, options.enable);
		});

        //Gets 'unpublish' messages on the socket in order to remove a stream from the room.
        socket.on('unpublish', function (streamId) {
            if (!socket.user.permissions[Permission.PUBLISH]) {
                callback('error', 'unauthorized');
                return;
            }
            var i, index;

            sendMsgToRoom(socket.room, 'onRemoveStream', {id: streamId});

            if (socket.room.streams[streamId].hasAudio() || socket.room.streams[streamId].hasVideo() || socket.room.streams[streamId].hasScreen()) {
                socket.state = 'sleeping';
                if (!socket.room.p2p) {
                    socket.room.webRtcController.removePublisher(streamId);
                }
            }

            index = socket.streams.indexOf(streamId);
            if (index !== -1) {
                socket.streams.splice(index, 1);
            }
            if (socket.room.streams[streamId]) {
                delete socket.room.streams[streamId];
            }

        });

        //Gets 'unsubscribe' messages on the socket in order to remove a subscriber from a determined stream (to).
        socket.on('unsubscribe', function (to) {
            if (!socket.user.permissions[Permission.SUBSCRIBE]) {
                callback('error', 'unauthorized');
                return;
            }
            if (socket.room.streams[to] === undefined) {
                return;
            }

            socket.room.streams[to].removeDataSubscriber(socket.id);

            if (socket.room.streams[to].hasAudio() || socket.room.streams[to].hasVideo() || socket.room.streams[to].hasScreen()) {
                if (!socket.room.p2p) {
                    socket.room.webRtcController.removeSubscriber(socket.id, to);
                };
            }

        });

        //When a client leaves the room mcuController removes its streams from the room if exists.
        socket.on('disconnect', function () {
            var i, index, id;

            logger.info('Socket disconnect ', socket.id);

            for (i in socket.streams) {
                if (socket.streams.hasOwnProperty(i)) {
                    sendMsgToRoom(socket.room, 'onRemoveStream', {id: socket.streams[i]});
                }
            }

            if (socket.room !== undefined) {

                for (i in socket.room.streams) {
                    if (socket.room.streams.hasOwnProperty(i)) {
                        socket.room.streams[i].removeDataSubscriber(socket.id);
                    }
                }

                index = socket.room.sockets.indexOf(socket.id);
                if (index !== -1) {
                    socket.room.sockets.splice(index, 1);
                }

                for (i in socket.streams) {
                    if (socket.streams.hasOwnProperty(i)) {
                        id = socket.streams[i];

                        if (socket.room.streams[id].hasAudio() || socket.room.streams[id].hasVideo() || socket.room.streams[id].hasScreen()) {
                            if (!socket.room.p2p) {
                                socket.room.webRtcController.removeClient(socket.id, id);
                            }

                        }

                        if (socket.room.streams[id]) {
                            delete socket.room.streams[id];
                        }
                    }
                }
            }

            if (socket.room !== undefined && socket.room.sockets.length === 0) {
                logger.info('Empty room ', socket.room.id, '. Scheduling for deletion.');
                socket.room.deleteTimer = setTimeout(function() {
                    logger.info('Deleting room', socket.room.id);
                    sipController.notify(socket.room, 'onRoomDeleted');
                    delete rooms[socket.room.id];
                    updateMyState('deleted', socket.room.id);
                }, 30000);
            }
        });
        
        socket.on('monitor', function(options, callback) {
            logger.info("Monitor stream ", options.streamId, " for user ", socket.id, ": ", options.op);
            if (options.op == 'dump') {
                callback('success', socket.room.streams);
            } else if (options.op == 'block') {
                var stream = socket.room.streams[options.streamId];
                socket.room.webRtcController.monitorStream(options.streamId, 'block:' + (options.enable ? "on" : "off"));
            }
        });
    });
};

/*
 * Gets a list of users in a determined room.
 */
exports.getUsersInRoom = function (room, callback) {
    "use strict";

    var users = [], sockets, id;
    if (rooms[room] === undefined) {
        callback(users);
        return;
    }

    sockets = rooms[room].sockets;

    for (id in sockets) {
        if (sockets.hasOwnProperty(id)) {
            users.push(findSocket(sockets[id]).user);
        }
    }

    callback(users);
};

/*
 * Delete a determined room.
 */
exports.deleteRoom = function (room, callback) {
    "use strict";

    var sockets, id;

    if (rooms[room] === undefined) {
        callback('Success');
        return;
    }
    sockets = rooms[room].sockets;

    for (id in sockets) {
        if (sockets.hasOwnProperty(id)) {
            rooms[room].webRtcController.removeClient(sockets[id]);
        }
    }
    logger.info('Deleting room ', room, rooms);
    delete rooms[room];
    updateMyState('deleted', room);
    logger.info('1 Deleting room ', room, rooms);
    callback('Success');
};

rpc.connect(function () {
    "use strict";

    addToCloudHandler(function () {

        var rpcID = 'mcuController_' + myId;

        rpc.bind(rpcID);

    });
});

sipController.init({rooms: rooms, dispatch: sendMsgToRoom});

listen();
