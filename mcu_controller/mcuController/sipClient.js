/**
 * Wrapper for node SIP client.
 */
var sip = require('sip');
var util = require('util');
var config = require('./../../mcu_config');


var opts;
var dialogs = {};
var calls = {};

var callbacks = {};

function rstring() {
    return Math.floor(Math.random()*1e6).toString();
}

function ack(rq) {
    // sending ACK
    sip.send({
      method: 'ACK',
      uri: rq.headers.contact[0].uri,
      headers: {
        to: rq.headers.to,
        from: rq.headers.from,
        'call-id': rq.headers['call-id'],
        cseq: {method: 'ACK', seq: rq.headers.cseq.seq},
        via: []
      }
    });
}

exports.start = function(_opts, inbound, hangup) {
    opts = _opts;
    callbacks = { inbound: inbound, hangup: hangup };

    var handleRequest = function(rq) {
      if(rq.headers.to.params.tag) { // check if it's an in dialog request
        var id = [rq.headers['call-id'], rq.headers.to.params.tag, rq.headers.from.params.tag].join(':');

        if(dialogs[id])
          dialogs[id](rq);
        else
          sip.send(sip.makeResponse(rq, 481, "Call doesn't exist"));
      } else if (rq.method == 'INVITE') {
          ack(rq);
          if (inbound != undefined) {
              inbound(rq);
          }
      } else if (rq.method == "BYE") {
          ack(rq);
          if (hangup != undefined) {
              hangup(rq);
          }
          sip.send(sip.makeResponse(rq, 200, "OK"));
      } else
        sip.send(sip.makeResponse(rq, 405, 'Method not allowed'));
    };

    sip.start({
        address: opts.host,
        port: opts.port,
        ws: false,
        logger : {
            send: function(m) { util.debug("send " + util.inspect(m, false, null)); },
            recv: function(m) { util.debug("recv " + util.inspect(m, false, null)); }
        }
    }, handleRequest);
};

exports.call = function(uri) {
    sip.send({
        method: 'INVITE',
        uri: uri,
      headers: {
        to: {uri: uri},
        from: {uri: 'sip:test@test', params: {tag: rstring()}},
        'call-id': rstring(),
        cseq: {method: 'INVITE', seq: rstring()},
        'content-type': 'application/sdp',
        contact: [{uri: 'sip:101@' + opts.address + ':' + opts.port}]
      },
      content:
        'v=0\r\n'+
        'o=- 13374 13374 IN IP4 192.168.1.101\r\n'+
        's=-\r\n'+
        'c=IN IP4 192.168.1.101\r\n'+
        't=0 0\r\n'+
        'm=audio 16424 RTP/AVP 96 101\r\n'+
        //'a=rtpmap:0 PCMU/8000\r\n'+
        'a=rtpmap:96 opus/48000/2\r\n'+
        'a=rtpmap:101 telephone-event/8000\r\n'+
        'a=fmtp:101 0-15\r\n'+
        'a=ptime:30\r\n'+
        'a=sendrecv\r\n'
    },
    function(rs) {
      if(rs.status >= 300) {
        console.log('call failed with status ' + rs.status);
      }
      else if(rs.status < 200) {
        console.log('call progress status ' + rs.status);
      }
      else {
        // yes we can get multiple 2xx response with different tags
        console.log('call answered with tag ' + rs.headers.to.params.tag);

        // sending ACK
        sip.send({
          method: 'ACK',
          uri: rs.headers.contact[0].uri,
          headers: {
            to: rs.headers.to,
            from: rs.headers.from,
            'call-id': rs.headers['call-id'],
            cseq: {method: 'ACK', seq: rs.headers.cseq.seq},
            via: []
          }
        });

        var id = [rs.headers['call-id'], rs.headers.from.params.tag, rs.headers.to.params.tag].join(':');

        // registering our 'dialog' which is just function to process in-dialog requests
        if(!dialogs[id]) {
          dialogs[id] = function(rq) {
            if(rq.method === 'BYE') {
              console.log('call received bye');

              delete dialogs[id];

              sip.send(sip.makeResponse(rq, 200, 'Ok'));
            }
            else {
              sip.send(sip.makeRespinse(rq, 405, 'Method not allowed'));
            }
          }
        }
      }
    });
};

exports.hangup = function(call) {
    sip.send({
      method: 'BYE',
      uri: call.headers.contact[0].uri,
      headers: {
        to: call.headers.from,
        from: call.headers.to,
        'call-id': call.headers['call-id'],
        cseq: {method: 'BYE', seq: rstring()},
        via: []
      }
    }, function(rs) {
      if(rs.status >= 300) {
        console.log('hangup failed with status ' + rs.status);
      }
      else if(rs.status < 200) {
        console.log('hangup progress status ' + rs.status);
      }
      else {
        console.log('hangup success');
      }
      callbacks.hangup(rs);
    });
};

exports.answer = function(rq, localSdp) {
    var resp = sip.makeResponse(rq, 200, "OK");
    resp.content = localSdp;
    sip.send(resp);
};

exports.reject = function(rq) {
    sip.send(sip.makeResponse(rq, 405, 'Method not allowed'));
};