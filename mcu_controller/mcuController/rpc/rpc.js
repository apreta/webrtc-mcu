var sys = require('util');
var amqp = require('amqp');
var rpcPublic = require('./rpcPublic');
var config = require('./../../../mcu_config');
var logger = require('./../logger').logger;

var TIMEOUT = 2000;

var corrID = 0;
var map = {};	//{corrID: {fn: callback, to: timeout}}
var connection;
var exc;
var clientQueue;
var connected = false;

var addr = {};

if (config.rabbit.url !== undefined) {
	addr.url = config.rabbit.url;
} else {
	addr.host = config.rabbit.host;
	addr.port = config.rabbit.port;
}

exports.connected = function() {
    return connected;
}

exports.connect = function(callback) {
    if (config.rabbit.testMode) {
        logger.info("Test mode, not using AMQP server");
        return;
    }

    logger.info("Connecting to AMQP server");


	// Create the amqp connection to rabbitMQ server
	connection = amqp.createConnection(addr);
	connection.on('ready', function () {
        logger.info("Connection to AMQP server ready");
        connected = true;

		// Create a direct exchange
		exc = connection.exchange(config.rabbit.prefix+'.rpcExchange', {type: 'direct', autoDelete: false}, function (exchange) {
			logger.info('Exchange ' + exchange.name + ' is open');

			// Create the queue for send messages
	  		clientQueue = connection.queue('', function (q) {
			  	logger.info('ClientQueue ' + q.name + ' is open');

			 	clientQueue.bind(config.rabbit.prefix+'.rpcExchange', clientQueue.name);

			  	clientQueue.subscribe(function (message) {
			  		if(map[message.corrID] !== undefined) {
			  			clearTimeout(map[message.corrID].to);
						map[message.corrID].fn(message.data);
						delete map[message.corrID];
					}
			  	});

			  	callback();
			});
		});
	});

    connection.on('error', function(e) {
        // ToDo: may not be AMQP error
        logger.error('AMQP error: ' + e);
        connected = false;
    });
}


exports.disconnect = function() {
    if (config.rabbit.testMode) return;

    if (connection) {
        connection.end();
        connected = false;
        delete connection;
    }
}


exports.bind = function(id, callback) {
    if (config.rabbit.testMode) return;

	//Create the queue for receive messages
    var name = config.rabbit.prefix+'.'+id;
	var q = connection.queue(name, function (queueCreated) {
	  	logger.info('Queue ' + queueCreated.name + ' is open');

	  	q.bind(config.rabbit.prefix+'.rpcExchange', id);

  		q.subscribe(function (message) {
    		rpcPublic[message.method](message.args, function(result) {
    			exc.publish(message.replyTo, {data: result, corrID: message.corrID});
    		});
  		});

  		if (callback) callback();

	});
}

/*
 * Calls remotely the 'method' function defined in rpcPublic of 'to'.
 */
exports.callRpc = function(to, method, args, callback) {

    /* WIP test mode */
    if (config.rabbit.testMode) {
        switch (method) {
            case "deleteToken":
                callback({room:config.rabbit.testRoom, host:config.rabbit.testHost, role:"presenter"});
                break;
            case 'addNewMcuController':
                callback({id:'1', ip:'127.0.0.1'});
                break;
            case 'keepAlive':
                callback("ok");
                break;
        }
        return;
    }

	corrID ++;
	map[corrID] = {};
	map[corrID].fn = callback;
	map[corrID].to = setTimeout(callbackError, TIMEOUT, corrID);

	var send = {method: method, args: args, corrID: corrID, replyTo: clientQueue.name };

 	exc.publish(to, send);

}

var callbackError = function(corrID) {
	map[corrID].fn('timeout');
	delete map[corrID];
}
