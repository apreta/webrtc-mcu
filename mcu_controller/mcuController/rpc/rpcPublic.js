var mcuController = require('./../mcuController');

/*
 * This function is called remotely from manager to get a list of the users in a determined room.
 */
exports.getUsersInRoom = function(id, callback) {

    mcuController.getUsersInRoom(id, function(users) {

        //console.log('Users for room ', id, ' are ', users);
        if(users == undefined) {
            callback('error');
        } else {
            callback(users);
        }
    });
}

exports.deleteRoom = function(roomId, callback) {
    mcuController.deleteRoom(roomId, function(result) {
        callback(result);
    });
}