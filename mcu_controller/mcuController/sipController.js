/**
 * Manage SIP phone calls, integrating their media with other webrtc calls.
 */

"use strict";

var ST = require('./Stream');
var config = require('./../../mcu_config');
var logger = require('./logger').logger;
var sip = require('./sipClient');
var esl = require('modesl');
var _ = require('underscore');

var rooms;
var calls = {};
var dispatch;


// Convert roapy sdp for sip
function convertSDP(answer) {
    var reg1 = new RegExp("^.*sdp\":\"(.*)\",.*$",'m');
    var sdp = answer.match(reg1);
    sdp = sdp[1].replace(/\\r\\n/g, "\r\n");
    return sdp;
}

function generateId() {
    var id = Math.floor(Math.random() * 1000000000000000000);
    return id;
}

// Called Number indicates room, original call id, and stream to connect to
// <room num>-<id of original call>-<id of stream subscribing to>
function parseDestination(to) {
    var match = /(?:sip:)?([0-9]*)-([0-9]*)-([0-9]*)(@)?/g.exec(to);
    if (!match) return null;
    return {
        number: match[1] + '-' + match[2] + '-' + match[3],
        room: match[1] ? rooms[match[1]] : null,
        conf_name: match[1] + '-' + match[2],
        id: match[2],
        to: match[3]
    };
}

// Conference name indicates room and id of original call
// <room num>-<id of original call>
function parseConfName(name) {
    var parsed = name.split('-');
    var room = rooms[parsed[0]];
    if (!room) return null;
    return {
        room: room,
        id: parsed[1],
        conn: room.connections[parsed[1]],
        conf_name: name
    };
}

// Caller Name contains unique generated steam id
// <stream id> or "<stream id>"
function parseId(str) {
    return str.replace(/(^")|("$)/g, '');
}


/* Handle SIP calls */
var incoming = function(req) {
    var id, st;

    logger.info('Call from ' + req.headers.from.uri);
    var sdp = '{"messageType":"OFFER","sdp":"'+req.content+'","offererSessionId":"200","last":"1"}';
    sdp = sdp.replace(/\r\n/g, '\\r\\n');

    // Figure out where to place call placed on "to" address...
    var id = parseId(req.headers.from.name);
    var info = parseDestination(req.headers.to.uri);
    var room = info.room;
    var callId = req.headers['call-id'];
    var conn = room.connections[info.id];
    var call = { request: req, room: room };

    if (id && room) {
        if (info.to === '0') {
            // Publish audio from phone conference to room
            room.webRtcController.addPublisher(id, sdp, function (answer) {
                var sdp = convertSDP(answer);

                sip.answer(req, sdp);
                calls[callId] = call;
                call.type = 'publish';
                call.id = id;

                st = new ST.Stream({id: id, audio: true, video: false, data: false, attributes: { sip: true }});
                call.from = id;
                call.room.streams[id] = st;

                conn.publisher = call;

            }, function() {
                // on ready (todo: make sure call is still valid)
                logger.info("Publishing call "+id);
                dispatch(room, 'onAddStream', st.getPublicStream());
            });
        } else {
            // Subscribe phone conference to other guest audio
            if (room.streams[info.to]) {
                room.webRtcController.addSubscriber(id, info.to, true, false, sdp, function(answer) {
                    var sdp = convertSDP(answer);

                    sip.answer(req, sdp);
                    calls[callId] = call;
                    call.type = 'subscribe';
                    call.id = id;
                    call.to = info.to;

                    conn.subscribers[info.to] = call;
                    logger.info('Subscribing call '+id+' to '+call.to);
                });
            } else {
                logger.error("Stream not found for sip call");
                sip.reject(req);
            }
        }
    } else {
        logger.error("Room not found for sip call");
        sip.reject(req);
    }
};


/* Handle SIP disconnects */
var hangup = function(req) {
    logger.info('Hangup from ' + req.headers.from.uri);
    var call = calls[req.headers['call-id']];
    if (call) {
        var streamId = call.id;
        var room = call.room;
        var conn = room.connections[streamId];

        if (room) {
            if (call.type == 'publish') {
                logger.info("Unpublishing call "+streamId);

                dispatch(room, 'onRemoveStream', {id: streamId});
                room.webRtcController.removePublisher(streamId);
            } else {
                logger.info("Unsubscribing call "+streamId+" from "+call.to);

                try {
                    room.webRtcController.removeSubscriber(streamId, call.to);
                } catch(e) {
                    logger.error('Error unsubscribing: ' + e);
                }
            }

            if (room.streams[streamId]) {
                delete room.streams[streamId];
            }
            if (conn && conn.subscribers[streamId]) {
                delete conn.subscribers[streamId];
            }
            if (conn && conn.publisher && conn.publisher.id == streamId) {
                conn.publisher = null;
            }
        }

        delete calls[req.headers['call-id']];
    }
};


/* Handle notification from main controller that room streams have changed */
exports.notify = function(room, type, stream) {
    if (!dispatch) return false;

    switch (type) {
        case 'onAddStream':
            logger.info("Added publisher " + stream.id);
            // Each phone call should be subscribed to new publisher
            _.each(room.connections, function(conn) {
                if (!conn.subscribers[stream.id] && conn.publisher.id != stream.id) {
                    var id = generateId();
                    var cmd =  conn.conf_name + ' dial {origination_caller_id_name='+id+'}sofia/external/' + conn.conf_name + '-' +
                               stream.id + '@' + config.sipController.sipAddress + ':' + config.sipController.sipPort;
                    conn.confCommand(cmd);
                }
            });
            break;

        case 'onRemoveStream':
            logger.info("Removed publisher " + stream.id);
            _.each(room.connections, function(conn) {
                if (conn.subscribers[stream.id]) {
                    var subId = conn.subscribers[stream.id].id;
                    conn.confCommand(conn.conf_name + ' hup ' + conn.memberIds[subId]);
                }
                /*if (conn.subscribers[stream.id]) {
                    sip.hangup(conn.subscribers[stream.id].request);
                }*/
            });
            break;

        case 'onRoomDeleted':
            logger.info('Cleaning up conferences on room deletion');
            _.each(room.connections, function(conn) {
                conn.execute('exit');
            });
            break;
    }
};


/* Start SIP client & connect to FreeSwitch */
exports.init = function(opts) {
    var api, restart;

    if (!config.sipController) {
        logger.info('No SIP configuration, not starting SIP client');
        return;
    }

    rooms = opts.rooms;
    dispatch = opts.dispatch;
    sip.start({	host: config.sipController.sipAddress, port: config.sipController.sipPort},
              incoming, hangup);
    logger.info("SIP client started");

    /*
     * Outbound connection to FS for receiving events and executing commands.
     */
    function connectESL() {
        restart = null;

        api = new esl.Connection(config.sipController.bridgeHost, config.sipController.bridgePort, config.sipController.bridgePassword, function() {
            api.events('json', 'CHANNEL_CREATE CHANNEL_DESTROY CUSTOM conference::maintenance');
            api.on('esl::event::**', function(ev) {
                if (ev.subclass == 'conference::maintenance') {
                    var action = ev.getHeader('Action');
                    logger.debug('Conf Event: ' + action);
                    switch (action) {
                        case "add-member": addMember(ev); break;
                        case "remove-member": removeMember(ev); break;
                    }
                } else {
                    logger.info('Event: ' + ev.type);
                }
            });
            api.on('esl::end', function(ev) {
                logger.info('ESL closed');
                if (!restart) {
                    restart = setTimeout(connectESL, 5000);
                }
            });
        });

        api.on('error', function(e) {
            logger.error('ESL error: ', e);
            if (!restart) {
                restart = setTimeout(connectESL, 5000);
            }
        });

        api.confCommand = function(cmd) {
            api.api('conference', cmd);
            logger.info('Conf command: ' + cmd);
        }
    }

    connectESL();

    function addMember(ev) {
        var memberId = ev.getHeader('Member-ID');
        var conf = parseConfName(ev.getHeader('Conference-Name'));
        if (conf.conn) {
            var conn = conf.conn;
            if (ev.getHeader('Member-Type') == 'moderator') {
                conn.master_id = memberId;
            } else {
                var id = parseId(ev.getHeader('Caller-Orig-Caller-ID-Name'));
                if (id) {
                    logger.info('New member ' + memberId + ' for stream ' + id);
                    conn.memberIds[id] = memberId;
                }

                var dest = parseDestination(ev.getHeader('Caller-Caller-ID-Number'));
                if (dest) {
                    if (dest.to == '0') {
                        // Phone to webrtc
                        conn.talker_id = memberId;
                        api.confCommand(dest.conf_name + ' mute ' + memberId);
                    } else {
                        // webrtc to phone
                        api.confCommand(dest.conf_name + ' deaf ' + memberId);
                        conn.listener_ids.push(memberId);
                        if (conn.talker_id) {
                            _.each(conn.listener_ids, function(id) {
                                api.confCommand(dest.conf_name + ' relate ' + conn.talker_id + ' ' + id + ' nohear');
                            });
                        }
                    }
                } else {
                    logger.error('Caller id missing');
                }
            }
        } else {
            logger.error('Connection info not found');
        }
    }

    function removeMember(ev) {
        var memberId = ev.getHeader('Member-ID');
        var conf = parseConfName(ev.getHeader('Conference-Name'));
        if (conf.conn) {
            var conn = conf.conn;
            if (conn.talker_id == memberId) { delete conn.talker_id; }
            else if (conn.master_id == memberId) { delete conn.master_id; }
            else {
                conn.listener_ids = _.without(conn.listener_ids, memberId);
            }

            var id = parseId(ev.getHeader('Caller-Orig-Caller-ID-Name'));
            if (id) {
                delete conn.memberIds[id];
            }
        }
    }

    /*
     * Handle inbound connections from FS, which are created when a call is connected to
     * us via the "socket" dial plan app.
     */

    var server = new esl.Server({port: config.sipController.appPort, myevents: true}, function() {
        logger.info("ESL server started");
    });

    server.on('connection::ready', function(conn, id) {
        var typeAhead, room, conf_name;
        var localId = generateId();

        logger.info('New call ' + id);
        conn.call_start = new Date().getTime();
        conn.subscribers = {};
        conn.publisher = null;
        conn.memberIds = {};
        conn.listener_ids = [];

        conn.confCommand = function(cmd) {
            conn.api('conference', cmd);
            logger.info('Conf command: ' + cmd);
        };

        function getPin(cb) {
            conn.execute('play_and_get_digits', "0 10 3 10000 # $${base_dir}/sounds/en/us/callie/conference/8000/conf-pin.wav invalid.wav pin \\d+", cb);
        }

        function checkPin(ev) {
            var pin = (typeAhead ? typeAhead : '') + ev.getHeader('variable_pin');
            typeAhead = null;
            if (pin == null) return;

            logger.info('Call ' + id + ' pin ' + pin);

            room = rooms[pin];
            if (!room) {
                // Someone needs to join via PC first to establish the room.
                conn.execute('set', 'playback_terminators=0123456789#*', function() {
                    conn.execute('playback', '$${base_dir}/sounds/en/us/callie/conference/8000/conf-bad-pin.wav', function(ev) {
                        typeAhead = ev.getHeader('variable_playback_terminator_used');
                        getPin(checkPin);
                    });
                });
            } else {
                joinConference(pin);
            }
        }

        function joinConference(pin) {
            conn.conf_name = pin + "-" + localId;

            if (!room.connections) room.connections = {};
            room.connections[localId] = conn;

            conn.execute('playback', 'tone_stream://%(200,0,500,600,700)', function() {
                conn.execute('conference', conn.conf_name+"+flags{moderator}", function(res) {
                    logger.debug("Conference app exit");
                });

                // Create second call connecting us to conference (todo: use bridge option?)
                var id = generateId();
                var cmd =  conn.conf_name + ' dial {origination_caller_id_name='+id+'}sofia/external/' + conn.conf_name + '-0@' +
                           config.sipController.sipAddress + ':' + config.sipController.sipPort;
                conn.confCommand(cmd);

                // And additional calls to listen to guests already in room
                _.each(room.streams, function(s) {
                    id = generateId();
                    logger.debug("Call subscribe " + s.getID());
                    var cmd =  conn.conf_name + ' dial {origination_caller_id_name='+id+'}sofia/external/' + conn.conf_name + '-' +
                               s.getID() + '@' + config.sipController.sipAddress + ':' + config.sipController.sipPort;
                    conn.confCommand(cmd);
                });
            });
        }

        // Call was answered in dial plan, prompt for PIN
        getPin(checkPin);

        conn.on('esl::end', function(evt, body) {
            this.call_end = new Date().getTime();
            var delta = (this.call_end - this.call_start) / 1000;
            logger.info("Call " + id + " duration " + delta + " seconds");

            // ToDo: reference count connection reference, or perhaps just keep until room is deleted
            if (room)
                delete room.connections[localId];

            // Outside call controls conference, so delete any related calls
            api.confCommand(conn.conf_name + ' hup all');
        });
    });

};

/*
var testId = '64';
var room = {};
room.id = testId;
room.sockets = [];
room.streams = {};
room.webRtcController = new controller.WebRtcController();
rooms[testId] = room;
*/
