/*global require, exports, console, setInterval, clearInterval*/

var addon = require('./../../mcuAPI/build/Release/addon');
var config = require('./../../mcu_config');
var logger = require('./logger').logger;

exports.WebRtcController = function () {
    "use strict";

    var that = {},
        // {id: array of subscribers}
        subscribers = {},
        // {id: OneToManyProcessor}
        publishers = {},
        // {id: stats info for stream}
        publisherStats = {},

        // {id: ExternalOutput}
        externalOutputs = {},

        INTERVAL_TIME_SDP = 100,
        INTERVAL_TIME_FIR = 100,
        INTERVAL_TIME_STATS = 2000,
        INTERVAL_FROZEN = 24000 / INTERVAL_TIME_STATS,
        waitForFIR,
        initWebRtcConnection,
        getSdp,
        getRoap,
        checkStats,
        resetTimer;

    /*
     * Given a WebRtcConnection waits for the state READY for ask it to send a FIR packet to its publisher. 
     */
    waitForFIR = function (wrtc, to) {

        if (publishers[to] !== undefined) {
            var intervarId = setInterval(function () {
              if (publishers[to]!==undefined){
                if (wrtc.getCurrentState() >= 2 && publishers[to].getPublisherState() >=2) {
                    publishers[to].sendFIR();
                    clearInterval(intervarId);
                }
              }

            }, INTERVAL_TIME_FIR);
        }
    };

    /*
     * Given a WebRtcConnection waits for the state CANDIDATES_GATHERED for set remote SDP. 
     */
    initWebRtcConnection = function (wrtc, sdp, callback, onReady) {

        wrtc.init();

        var roap = sdp,
            remoteSdp = getSdp(roap);

        wrtc.setRemoteSdp(remoteSdp);

        var sdpDelivered = false;

        var intervarId = setInterval(function () {

                var state = wrtc.getCurrentState(), localSdp, answer;

                if (state == 1 && !sdpDelivered) {
                    localSdp = wrtc.getLocalSdp();

                    answer = getRoap(localSdp, roap);
                    callback(answer);
                    sdpDelivered = true;

                }
                if (state == 2) {
                    if (onReady != undefined) {
                        onReady();
                    }
                }

                if (state >= 2) {
                    clearInterval(intervarId);
                    logger.info("webrtc session setup done");
                }

            }, INTERVAL_TIME_SDP);
    };

    /*
     * Gets SDP from roap message.
     */
    getSdp = function (roap) {

        var reg1 = new RegExp(/^.*sdp\":\"(.*)\",.*$/),
            sdp = roap.match(reg1)[1],
            reg2 = new RegExp(/\\r\\n/g);

        sdp = sdp.replace(reg2, '\n');

        return sdp;

    };

    /*
     * Gets roap message from SDP.
     */
    getRoap = function (sdp, offerRoap) {

        var reg1 = new RegExp(/\n/g),
            offererSessionId = offerRoap.match(/("offererSessionId":)(.+?)(,)/)[0],
            answererSessionId = "106",
            answer = ('\n{\n \"messageType\":\"ANSWER\",\n');
            answer = ('\n{\n \"messageType\":\"ANSWER\",\n');

        sdp = sdp.replace(reg1, '\\r\\n');

        //var reg2 = new RegExp(/^.*offererSessionId\":(...).*$/);
        //var offererSessionId = offerRoap.match(reg2)[1];

        answer += ' \"sdp\":\"' + sdp + '\",\n';
        //answer += ' \"offererSessionId\":' + offererSessionId + ',\n';
        answer += ' ' + offererSessionId + '\n';
        answer += ' \"answererSessionId\":' + answererSessionId + ',\n \"seq\" : 1\n}\n';

        return answer;
    };

    checkStats = function(wrtc, from, onNotify) {

        var stats = publisherStats[from];
        var curr = wrtc.getStats();
        if (stats.received != curr.received) {
            stats.received = curr.received;
            stats.sent = curr.sent;
            stats.missed = 0;
            if (stats.reportedTime != undefined) {
                logger.info("Publisher ", from, " appears unfrozen");
                stats.reportedTime = undefined;
                if (onNotify) onNotify('unfrozen', {stats:stats, id:from});
            }
        } else if (++stats.missed > INTERVAL_FROZEN && stats.reportedTime == undefined) {
            logger.info("Publisher ", from, " appears frozen");
            stats.reportedTime = new Date();
            if (onNotify) onNotify('frozen', {stats:stats, id:from});
        }
    };

    that.addExternalInput = function (from, url, callback) {

        if (publishers[from] === undefined) {

            logger.info("Adding external input peer_id ", from);

            var muxer = new addon.OneToManyProcessor(),
                ei = new addon.ExternalInput(url);

            publishers[from] = muxer;
            subscribers[from] = [];

            ei.setAudioReceiver(muxer);
            ei.setVideoReceiver(muxer);
            muxer.setExternalPublisher(ei);

            var answer = ei.init();

            if (answer >= 0) {
                callback('success');
            } else {
                callback(answer);
            }

        } else {
            logger.info("Publisher already set for", from);
        }
    };

    that.addExternalOutput = function (to, url) {
        if (publishers[to] !== undefined) {
            logger.info("Adding ExternalOutput to " + to + " url " + url);
            var externalOutput = new addon.ExternalOutput(url);
            externalOutput.init();
            publishers[to].addExternalOutput(externalOutput, url);
            externalOutputs[url] = externalOutput;
        }

    };

    that.removeExternalOutput = function (to, url) {
      if (externalOutputs[url] !== undefined && publishers[to]!=undefined) {
        logger.info("Stopping ExternalOutput: url " + url);
        publishers[to].removeSubscriber(url);
        delete externalOutputs[url];
      }
    };

    /*
     * Adds a publisher to the room. This creates a new OneToManyProcessor
     * and a new WebRtcConnection. This WebRtcConnection will be the publisher
     * of the OneToManyProcessor.
     */
    that.addPublisher = function (from, sdp, callback, onReady, onNotify) {

        if (publishers[from] === undefined) {

            logger.info("Adding publisher peer_id ", from);

            var muxer = new addon.OneToManyProcessor(),
                wrtc = new addon.WebRtcConnection(true, true, config.mcu.stunserver, config.mcu.stunport, config.mcu.localaddress, config.mcu.minport, config.mcu.maxport);

            publishers[from] = muxer;
            subscribers[from] = [];

            wrtc.setAudioReceiver(muxer);
            wrtc.setVideoReceiver(muxer);
            muxer.setPublisher(wrtc);

            var startStats = function() {
                logger.info("Starting stats check for ", from);
                if (publisherStats[from]) {
                    clearInterval(publisherStats[from].timer);
                }
                publisherStats[from] = {
                    missed: 0,
                    received: 0,
                    sent: 0,
                    timer: setInterval(function() {
                        checkStats(wrtc, from, onNotify);
                    }, INTERVAL_TIME_STATS)
                };
                onReady.apply(this, arguments);
            };
            
            initWebRtcConnection(wrtc, sdp, callback, startStats);


            //logger.info('Publishers: ', publishers);
            //logger.info('Subscribers: ', subscribers);

        } else {
            logger.info("Publisher already set for", from);
        }
    };

    /*
     * Adds a subscriber to the room. This creates a new WebRtcConnection.
     * This WebRtcConnection will be added to the subscribers list of the
     * OneToManyProcessor.
     */
    that.addSubscriber = function (from, to, audio, video, sdp, callback) {

        if (publishers[to] !== undefined && subscribers[to].indexOf(from) === -1 && sdp.match('OFFER') !== null) {

            logger.info("Adding subscriber from ", from, 'to ', to);

            if (audio === undefined) audio = true;
            if (video === undefined) video = true;

            var wrtc = new addon.WebRtcConnection(audio, video, config.mcu.stunserver, config.mcu.stunport, config.mcu.localaddress, config.mcu.minport, config.mcu.maxport);

            subscribers[to].push(from);
            publishers[to].addSubscriber(wrtc, from);
            
            var sendFir = function() {
                if (video && publishers[to]) {
				    logger.info("Sending FIR request");
                    publishers[to].sendFIR();
                }
            };
            
            initWebRtcConnection(wrtc, sdp, callback, sendFir);
//            waitForFIR(wrtc, to);

            //logger.info('Publishers: ', publishers);
            //logger.info('Subscribers: ', subscribers);
        }
    };

    /*
     * Removes a publisher from the room. This also deletes the associated OneToManyProcessor.
     */
    that.removePublisher = function (from) {

        if (publisherStats[from]) {
            clearInterval(publisherStats[from].timer);
        }
        
        if (subscribers[from] !== undefined && publishers[from] !== undefined) {
            logger.info('Removing muxer', from);
            publishers[from].close();
            logger.info('Removing subscribers', from);
            delete subscribers[from];
            logger.info('Removing publisher', from);
            delete publishers[from];
            logger.info('Removed all');
        }
    };

    /*
     * Removes a subscriber from the room. This also removes it from the associated OneToManyProcessor.
     */
    that.removeSubscriber = function (from, to) {

        // If for some reason the publisher is removed first, the subscriber list will have been removed
        if (subscribers[to] !== undefined) {
            var index = subscribers[to].indexOf(from);
            if (index !== -1) {
                logger.info('Removing subscriber ', from, 'to muxer ', to);
                publishers[to].removeSubscriber(from);
                subscribers[to].splice(index, 1);
            }
        }
    };

    /*
     * Removes a client from the session. This removes the publisher and all the subscribers related.
     */
    that.removeClient = function (from, streamId) {

        var key, index;

        if (publisherStats[streamId]) {
            clearInterval(publisherStats[streamId].timer);
        }

        logger.info('Removing client ', from);
        for (key in subscribers) {
            if (subscribers.hasOwnProperty(key)) {
                index = subscribers[key].indexOf(from);
                if (index !== -1) {
                    logger.info('Removing subscriber ', from, ' to muxer ', key);
                    publishers[key].removeSubscriber(from);
                    subscribers[key].splice(index, 1);
                }
            }
        }

        if (subscribers[streamId] !== undefined && publishers[streamId] !== undefined) {
            logger.info('Removing muxer ', streamId);
            publishers[streamId].close();
            delete subscribers[streamId];
            delete publishers[streamId];
        }
    };
    
    /*
     * Pause/unpause video stream to a particular user
     */
	that.pauseVideoStream = function(from, to, enable) {
        if (publishers[to] !== undefined && subscribers[to].indexOf(from) !== -1) {
            logger.info("Pause video stream from ", from, 'to ', to);
            var muxer = publishers[to];
            muxer.pauseVideo(from, enable);
        }
        
        if (!enable) {
			if (resetTimer) clearTimeout(resetTimer);
			resetTimer = setTimeout(function() {
				logger.info("Sending FIR request");
				publishers[to].sendFIR();
			}, INTERVAL_TIME_FIR);
		}
	};

    /*
     * Pause/unpause audio stream to a particular user
     */
	that.pauseAudioStream = function(from, to, enable) {
        if (publishers[to] !== undefined && subscribers[to].indexOf(from) !== -1) {
            logger.info("Pause audio stream from ", from, 'to ', to);
            var muxer = publishers[to];
            muxer.pauseAudio(from, enable);
        }
	};

    that.monitorStream = function(from, op) {
        logger.info("Monitor stream op ", op, " for ", from);
        var muxer = publishers[from];
        if (muxer) {
            muxer.monitorStream('', op);
        }
    };

    return that;
};
