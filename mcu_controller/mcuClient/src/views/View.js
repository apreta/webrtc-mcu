/*
 * View class represents a HTML component
 * Every view is an EventDispatcher.
 */
var Mcu = Mcu || {};
Mcu.View = function (spec) {
	"use strict";

    var that = Mcu.EventDispatcher({});

    // Variables

    // URL where it will look for icons and assets
    that.url = "http://apreta.com/video";
    return that;
};