/*global window, console, navigator*/

var Mcu = Mcu || {};

Mcu.sessionId = 103;

Mcu.Connection = function (spec) {
    "use strict";
    var that = {};

    spec.session_id = (Mcu.sessionId += 1);

    // Check which WebRTC Stack is installed.
    that.browser = "";

    if (typeof module !== 'undefined' && module.exports) {
        L.Logger.error('Publish/subscribe video/audio streams not supported in mcufc yet');
        that = Mcu.FcStack(spec);
    } else if (window.navigator.userAgent.match("Firefox") !== null) {
        // Firefox
        that.browser = "mozilla";
        that = Mcu.FirefoxStack(spec);
    } else if (window.navigator.userAgent.toLowerCase().indexOf("bowser") >= 0) {
        // Bowser
        L.Logger.debug("Bowser!");
        that = Mcu.BowserStack(spec);
        that.browser = "bowser";
    } else if (window.navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
        if (window.navigator.appVersion.match(/Chrome\/([\w\W]*?)\./)[1] <= 31) {
            // Google Chrome Stable.
            L.Logger.debug("Stable!");
            that = Mcu.ChromeStableStack(spec);
            that.browser = "chrome-stable";
        } else {
            // Google Chrome Canary.
            L.Logger.debug("Canary!");
            that = Mcu.ChromeCanaryStack(spec);
            that.browser = "chrome-canary";
        }
    } else if (navigator.webkitGetUserMedia !== undefined) {
        // Try with bowser config (e.g. could be Safari with openwebrtc plugin)
        L.Logger.debug("Bowser fallback");
        that = Mcu.BowserStack(spec);
        that.browser = "bowser";
    } else {
        // None.
        that.browser = "none";
        throw "WebRTC stack not available";
    }

    return that;
};

Mcu.GetUserMedia = function (config, callback, error) {
    "use strict";

    navigator.getMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

    if (typeof module !== 'undefined' && module.exports) {
        L.Logger.error('Video/audio streams not supported in mcufc yet');
    } else {
        navigator.getMedia(config, callback, error);
    }
};
