#ifndef WEBRTCCONNECTION_H
#define WEBRTCCONNECTION_H

#include <node.h>
#include <WebRtcConnection.h>
#include "MediaDefinitions.h"
#include "OneToManyProcessor.h"


/*
 * Wrapper class of mcu::WebRtcConnection
 *
 * A WebRTC Connection. This class represents a WebRtcConnection that can be established with other peers via a SDP negotiation
 * it comprises all the necessary ICE and SRTP components.
 */
class WebRtcConnection : public node::ObjectWrap {
 public:
  static void Init(v8::Handle<v8::Object> exports);

  mcu::WebRtcConnection *me;

 private:
  WebRtcConnection();
  ~WebRtcConnection();

  static v8::Persistent<v8::Function> constructor;

  /*
   * Constructor.
   * Constructs an empty WebRtcConnection without any configuration.
   */
  static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Closes the webRTC connection.
   * The object cannot be used after this call.
   */
  static void close(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Inits the WebRtcConnection by starting ICE Candidate Gathering.
   * Returns true if the candidates are gathered.
   */
  static void init(const v8::FunctionCallbackInfo<v8::Value>& args);  
  /*
   * Sets the SDP of the remote peer.
   * Param: the SDP.
   * Returns true if the SDP was received correctly.
   */
  static void setRemoteSdp(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Obtains the local SDP.
   * Returns the SDP as a string.
   */
  static void getLocalSdp(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Sets a MediaReceiver that is going to receive Audio Data
   * Param: the MediaReceiver to send audio to.
   */
  static void setAudioReceiver(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Sets a MediaReceiver that is going to receive Video Data
   * Param: the MediaReceiver
   */
  static void setVideoReceiver(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Gets the current state of the Ice Connection
   * Returns the state.
   */
  static void getCurrentState(const v8::FunctionCallbackInfo<v8::Value>& args);
  /*
   * Gets statistics from connection.
   * Returns statistics.
   */
  static void getStats(const v8::FunctionCallbackInfo<v8::Value>& args);

};

#endif
