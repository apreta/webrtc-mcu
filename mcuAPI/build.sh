#!/bin/bash

if [ -z "$MCU_HOME" ]; then
	export MCU_HOME=`pwd`/../mcu
fi

rm -rf build

# Currently using older Boost on Mac to match Ubuntu 14.04
export CXXFLAGS="-I /usr/local/opt/boost155/include"

#if hash node-waf 2>/dev/null; then
#  echo 'building with node-waf'
#  node-waf configure build
#else
  echo 'building with node-gyp'
  node-gyp rebuild
#fi
