#ifndef BUILDING_NODE_EXTENSION
#define BUILDING_NODE_EXTENSION
#endif

#include <node.h>
#include "WebRtcConnection.h"


using namespace v8;

WebRtcConnection::WebRtcConnection() {};
WebRtcConnection::~WebRtcConnection() {};

Persistent<Function> WebRtcConnection::constructor;

void WebRtcConnection::Init(Handle<Object> exports) {
  Isolate* isolate = Isolate::GetCurrent();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "WebRtcConnection"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", close);
  NODE_SET_PROTOTYPE_METHOD(tpl, "init", init);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setRemoteSdp", setRemoteSdp);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getLocalSdp", getLocalSdp);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setAudioReceiver", setAudioReceiver);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setVideoReceiver", setVideoReceiver);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getCurrentState", getCurrentState);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getStats", getStats);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "WebRtcConnection"), tpl->GetFunction());
}

void WebRtcConnection::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  if (args.Length()<4){
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Wrong number of arguments")));
  }

  if (!args.IsConstructCall()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Must use new")));
  }
  
  bool a = (args[0]->ToBoolean())->BooleanValue();
  bool v = (args[1]->ToBoolean())->BooleanValue();
  String::Utf8Value param(args[2]->ToString());
  std::string stunServer = std::string(*param);
  int stunPort = args[3]->IntegerValue();
  String::Utf8Value param2(args[4]->ToString());
  std::string localAddress = std::string(*param2);
  int minPort = args[5]->IntegerValue();
  int maxPort = args[6]->IntegerValue();

  WebRtcConnection* obj = new WebRtcConnection();
  obj->me = new mcu::WebRtcConnection(a, v, stunServer,stunPort,localAddress,minPort,maxPort);
  obj->Wrap(args.This());

  args.GetReturnValue().Set(args.This());
}

// N.B.: if this instance is handed to a OneToManyProcessor muxer then it has
// ownership and this API should not be called.
void WebRtcConnection::close(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  //delete me;
  if (me != NULL) {
    me->close();
    me->release();
    me = NULL;
  }

  args.GetReturnValue().SetNull();
}

void WebRtcConnection::init(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;
  
  bool r = me != NULL ? me->init() : false;

  args.GetReturnValue().Set(r);
}

void WebRtcConnection::setRemoteSdp(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  String::Utf8Value param(args[0]->ToString());
  std::string sdp = std::string(*param);

  bool r = me != NULL ? me->setRemoteSdp(sdp) : false;

  args.GetReturnValue().Set(r);
}

void WebRtcConnection::getLocalSdp(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  std::string sdp = me != NULL ? me->getLocalSdp() : "already_closed";

  args.GetReturnValue().Set(String::NewFromUtf8(isolate, sdp.c_str()));
}


void WebRtcConnection::setAudioReceiver(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
    
  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  MediaSink* param = ObjectWrap::Unwrap<MediaSink>(args[0]->ToObject());
  mcu::MediaSink *mr = param->msink;

  if (me != NULL)
    me-> setAudioSink(mr);

  args.GetReturnValue().SetNull();
}

void WebRtcConnection::setVideoReceiver(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  MediaSink* param = ObjectWrap::Unwrap<MediaSink>(args[0]->ToObject());
  mcu::MediaSink *mr = param->msink;

  if (me != NULL)
    me-> setVideoSink(mr);

  args.GetReturnValue().SetNull();
}

void WebRtcConnection::getCurrentState(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  int state = me != NULL ? me->getCurrentState() : -1;

  args.GetReturnValue().Set(state);
}

void WebRtcConnection::getStats(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  WebRtcConnection* obj = ObjectWrap::Unwrap<WebRtcConnection>(args.Holder());
  mcu::WebRtcConnection *me = obj->me;

  unsigned int received, sent;
  if (me != NULL)
    me->getStats(sent, received);
  else
    received = sent = -1;

  Local<Object> stats = Object::New(isolate);
  stats->Set(String::NewFromUtf8(isolate, "received"), Number::New(isolate, received));
  stats->Set(String::NewFromUtf8(isolate, "sent"), Number::New(isolate, sent));
  
  args.GetReturnValue().Set(stats);
}
