#ifndef BUILDING_NODE_EXTENSION
#define BUILDING_NODE_EXTENSION
#endif

#include <node.h>
#include "OneToManyProcessor.h"


using namespace v8;

Persistent<Function> OneToManyProcessor::constructor;

OneToManyProcessor::OneToManyProcessor() {};
OneToManyProcessor::~OneToManyProcessor() {};

void OneToManyProcessor::Init(Handle<Object> exports) {
  Isolate* isolate = Isolate::GetCurrent();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate,"OneToManyProcessor"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", close);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setPublisher", setPublisher);
  NODE_SET_PROTOTYPE_METHOD(tpl, "addExternalOutput", addExternalOutput);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setExternalPublisher", setExternalPublisher);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getPublisherState", getPublisherState);
  NODE_SET_PROTOTYPE_METHOD(tpl, "hasPublisher", hasPublisher);
  NODE_SET_PROTOTYPE_METHOD(tpl, "addSubscriber", addSubscriber);
  NODE_SET_PROTOTYPE_METHOD(tpl, "removeSubscriber", removeSubscriber);
  NODE_SET_PROTOTYPE_METHOD(tpl, "sendFIR", sendFIR);
  NODE_SET_PROTOTYPE_METHOD(tpl, "pauseVideo", pauseVideo);
  NODE_SET_PROTOTYPE_METHOD(tpl, "pauseAudio", pauseAudio);
  NODE_SET_PROTOTYPE_METHOD(tpl, "monitorStream", monitorStream);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "OneToManyProcessor"), tpl->GetFunction());
}

void OneToManyProcessor::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  if (!args.IsConstructCall()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Must use new")));
  }

  OneToManyProcessor* obj = new OneToManyProcessor();
  obj->me = new mcu::OneToManyProcessor();
  obj->msink = obj->me;

  obj->Wrap(args.This());

  args.GetReturnValue().Set(args.This());
}

void OneToManyProcessor::close(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  //delete me;
  if (me != NULL) {
    me->close();
    me->release();
    me = NULL;
  }

  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::setPublisher(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  WebRtcConnection* param = ObjectWrap::Unwrap<WebRtcConnection>(args[0]->ToObject());
  mcu::WebRtcConnection* wr = (mcu::WebRtcConnection*)param->me;

  if (me != NULL && wr != NULL) {
    mcu::MediaSource* ms = dynamic_cast<mcu::MediaSource*>(wr);
  
    me->setPublisher(ms);
  }

  args.GetReturnValue().SetNull();
}
void OneToManyProcessor::setExternalPublisher(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  ExternalInput* param = ObjectWrap::Unwrap<ExternalInput>(args[0]->ToObject());
  mcu::ExternalInput* wr = (mcu::ExternalInput*)param->me;

  if (me != NULL && wr != NULL) {
    mcu::MediaSource* ms = dynamic_cast<mcu::MediaSource*>(wr);
  
    me->setPublisher(ms);
  }

  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::getPublisherState(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  int state = -1;

  if (me != NULL) {
    mcu::MediaSource * ms = me->publisher;

    mcu::WebRtcConnection* wr = (mcu::WebRtcConnection*)ms;

    state = wr->getCurrentState();
  }

  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::hasPublisher(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  bool p = true;

  if(me && me->publisher == NULL) {
    p = false;
  }
  
  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::addSubscriber(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  WebRtcConnection* param = ObjectWrap::Unwrap<WebRtcConnection>(args[0]->ToObject());
  mcu::WebRtcConnection* wr = param->me;

  if (me != NULL && wr != NULL) {
    mcu::MediaSink* ms = dynamic_cast<mcu::MediaSink*>(wr);

    v8::String::Utf8Value param1(args[1]->ToString());
    std::string peerId = std::string(*param1);

    me->addSubscriber(ms, peerId);
  }

  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::addExternalOutput(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  ExternalOutput* param = ObjectWrap::Unwrap<ExternalOutput>(args[0]->ToObject());
  mcu::ExternalOutput* wr = param->me;

  if (me != NULL && wr != NULL) {
    mcu::MediaSink* ms = dynamic_cast<mcu::MediaSink*>(wr);

    v8::String::Utf8Value param1(args[1]->ToString());
    std::string peerId = std::string(*param1);

    me->addSubscriber(ms, peerId);
  }

  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::removeSubscriber(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  v8::String::Utf8Value param1(args[0]->ToString());
  std::string peerId = std::string(*param1);
  
  if (me != NULL)
    me->removeSubscriber(peerId);

  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::sendFIR(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  if (me != NULL)
    me->publisher->sendFirPacket();
  
  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::pauseVideo(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  v8::String::Utf8Value param1(args[0]->ToString());
  std::string peerId = std::string(*param1);
  
  bool enable = args[1]->ToBoolean()->IsTrue();

  if (me != NULL)
    me->pauseVideo(peerId, enable);
  
  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::pauseAudio(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  v8::String::Utf8Value param1(args[0]->ToString());
  std::string peerId = std::string(*param1);
  
  bool enable = args[1]->ToBoolean()->IsTrue();

  if (me != NULL)
    me->pauseAudio(peerId, enable);
  
  args.GetReturnValue().SetNull();
}

void OneToManyProcessor::monitorStream(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyProcessor* obj = ObjectWrap::Unwrap<OneToManyProcessor>(args.Holder());
  mcu::OneToManyProcessor *me = (mcu::OneToManyProcessor*)obj->me;

  v8::String::Utf8Value param1(args[0]->ToString());
  v8::String::Utf8Value param2(args[1]->ToString());
  std::string peerId = std::string(*param1);
  std::string op = std::string(*param2);

  std::string res = me != NULL ? me->monitorStream(peerId, op) : "already_closed";

  args.GetReturnValue().Set(String::NewFromUtf8(isolate, res.c_str()));
}