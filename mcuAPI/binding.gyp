{
  'variables': {
    "prefers_libcpp":"<!(python -c \"import os;import platform;u=platform.uname();print((u[0] == 'Darwin' and int(u[2][0:2]) >= 13) and '-stdlib=libstdc++' not in os.environ.get('CXXFLAGS','') and '-mmacosx-version-min' not in os.environ.get('CXXFLAGS',''))\")"
  },
  'targets': [
  {
    'target_name': 'addon',
      'sources': [ 'addon.cc', 'WebRtcConnection.cc', 'OneToManyProcessor.cc', 'ExternalInput.cc', 'ExternalOutput.cc', 'OneToManyTranscoder.cc' ],
      'include_dirs' : ['$(MCU_HOME)/src/mcu', '$(MCU_HOME)/../build/libdeps/build/include', '.'],
      'libraries': ['-L$(MCU_HOME)/build/mcu', '-lmcu'],
      'conditions': [
        [ 'OS=="mac"', {
          'xcode_settings': {
            'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',        # -fno-exceptions
              'GCC_ENABLE_CPP_RTTI': 'YES',              # -fno-rtti
              'OTHER_CFLAGS': [
              '-g -O3'
            ],
          },
        }, { # OS!="mac"
          'cflags!' : ['-fno-exceptions'],
          'cflags_cc' : ['-Wall', '-O3', '-g'],
          'cflags_cc!' : ['-fno-exceptions']

        }],
        [ '"<(prefers_libcpp)"=="True"', {
            'xcode_settings': {
              'MACOSX_DEPLOYMENT_TARGET':'10.9'
            }
          }
        ]
      ]
  }
  ]
}
