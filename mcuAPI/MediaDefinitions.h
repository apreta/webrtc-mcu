#ifndef MEDIARECEIVER_H
#define MEDIARECEIVER_H

#include <node.h>
#include <node_object_wrap.h>

#include <MediaDefinitions.h>


/*
 * Wrapper class of mcu::MediaReceiver
 */
class MediaSink : public node::ObjectWrap {
public:

  mcu::MediaSink* msink;
};


#endif
