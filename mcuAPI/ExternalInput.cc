#ifndef BUILDING_NODE_EXTENSION
#define BUILDING_NODE_EXTENSION
#endif
#include <node.h>
#include "ExternalInput.h"


using namespace v8;

Persistent<Function> ExternalInput::constructor;

ExternalInput::ExternalInput() {};
ExternalInput::~ExternalInput() {};

void ExternalInput::Init(Handle<Object> exports) {
  Isolate* isolate = Isolate::GetCurrent();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "ExternalInput"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", close);
  NODE_SET_PROTOTYPE_METHOD(tpl, "init", init);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setAudioReceiver", setAudioReceiver);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setVideoReceiver", setVideoReceiver);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "ExternalInput"), tpl->GetFunction());
}

void ExternalInput::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  if (args.Length()<1){
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Wrong number of arguments")));
  }
    
  if (!args.IsConstructCall()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Must use new")));
  }
  
  v8::String::Utf8Value param(args[0]->ToString());
  std::string url = std::string(*param);

  ExternalInput* obj = new ExternalInput();
  obj->me = new mcu::ExternalInput(url);

  obj->Wrap(args.This());

  args.GetReturnValue().Set(args.This());
}

void ExternalInput::close(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  ExternalInput* obj = ObjectWrap::Unwrap<ExternalInput>(args.Holder());
  mcu::ExternalInput *me = (mcu::ExternalInput*)obj->me;

  delete me;

  args.GetReturnValue().SetNull();
}

void ExternalInput::init(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  ExternalInput* obj = ObjectWrap::Unwrap<ExternalInput>(args.Holder());
  mcu::ExternalInput *me = (mcu::ExternalInput*)obj->me;

  int r = me->init();

  args.GetReturnValue().Set(r);
}

void ExternalInput::setAudioReceiver(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  ExternalInput* obj = ObjectWrap::Unwrap<ExternalInput>(args.Holder());
  mcu::ExternalInput *me = (mcu::ExternalInput*)obj->me;

  MediaSink* param = ObjectWrap::Unwrap<MediaSink>(args[0]->ToObject());
  mcu::MediaSink *mr = param->msink;

  me->setAudioSink(mr);

  args.GetReturnValue().SetNull();
}

void ExternalInput::setVideoReceiver(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  ExternalInput* obj = ObjectWrap::Unwrap<ExternalInput>(args.Holder());
  mcu::ExternalInput *me = (mcu::ExternalInput*)obj->me;

  MediaSink* param = ObjectWrap::Unwrap<MediaSink>(args[0]->ToObject());
  mcu::MediaSink *mr = param->msink;

  me->setVideoSink(mr);

  args.GetReturnValue().SetNull();
}
