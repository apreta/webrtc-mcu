#ifndef BUILDING_NODE_EXTENSION
#define BUILDING_NODE_EXTENSION
#endif
#include <node.h>
#include "ExternalOutput.h"


using namespace v8;

Persistent<Function> ExternalOutput::constructor;

ExternalOutput::ExternalOutput() {};
ExternalOutput::~ExternalOutput() {};

void ExternalOutput::Init(Handle<Object> exports) {
  Isolate* isolate = Isolate::GetCurrent();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "ExternalOutput"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", close);
  NODE_SET_PROTOTYPE_METHOD(tpl, "init", init);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "ExternalOutput"), tpl->GetFunction());
}

void ExternalOutput::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  if (args.Length()<1){
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Wrong number of arguments")));
  }
  
  if (!args.IsConstructCall()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Must use new")));
  }

  v8::String::Utf8Value param(args[0]->ToString());
  std::string url = std::string(*param);

  ExternalOutput* obj = new ExternalOutput();
  obj->me = new mcu::ExternalOutput(url);

  obj->Wrap(args.This());

  args.GetReturnValue().Set(args.This());
}

void ExternalOutput::close(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  ExternalOutput* obj = ObjectWrap::Unwrap<ExternalOutput>(args.Holder());
  mcu::ExternalOutput *me = (mcu::ExternalOutput*)obj->me;

  delete me;

  args.GetReturnValue().SetNull();
}

void ExternalOutput::init(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  ExternalOutput* obj = ObjectWrap::Unwrap<ExternalOutput>(args.Holder());
  mcu::ExternalOutput *me = (mcu::ExternalOutput*)obj->me;

  int r = me->init();

  args.GetReturnValue().Set(r);
}

