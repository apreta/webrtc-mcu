#ifndef BUILDING_NODE_EXTENSION
#define BUILDING_NODE_EXTENSION
#endif
#include <node.h>
#include "OneToManyTranscoder.h"


using namespace v8;

Persistent<Function> OneToManyTranscoder::constructor;

OneToManyTranscoder::OneToManyTranscoder() {};
OneToManyTranscoder::~OneToManyTranscoder() {};

void OneToManyTranscoder::Init(Handle<Object> exports) {
  Isolate* isolate = Isolate::GetCurrent();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate,"OneToManyTranscoder"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "close", close);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setPublisher", setPublisher);
  NODE_SET_PROTOTYPE_METHOD(tpl, "hasPublisher", hasPublisher);
  NODE_SET_PROTOTYPE_METHOD(tpl, "addSubscriber", addSubscriber);
  NODE_SET_PROTOTYPE_METHOD(tpl, "removeSubscriber", removeSubscriber);
  NODE_SET_PROTOTYPE_METHOD(tpl, "sendFIR", sendFIR);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "OneToManyTranscoder"), tpl->GetFunction());
}

void OneToManyTranscoder::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  if (!args.IsConstructCall()) {
    isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Must use new")));
  }

  OneToManyTranscoder* obj = new OneToManyTranscoder();
  obj->me = new mcu::OneToManyTranscoder();
  obj->msink = obj->me;

  obj->Wrap(args.This());

  args.GetReturnValue().Set(args.This());
}

void OneToManyTranscoder::close(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyTranscoder* obj = ObjectWrap::Unwrap<OneToManyTranscoder>(args.Holder());
  mcu::OneToManyTranscoder *me = (mcu::OneToManyTranscoder*)obj->me;

  delete me;

  args.GetReturnValue().SetNull();
}

void OneToManyTranscoder::setPublisher(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyTranscoder* obj = ObjectWrap::Unwrap<OneToManyTranscoder>(args.Holder());
  mcu::OneToManyTranscoder *me = (mcu::OneToManyTranscoder*)obj->me;

  WebRtcConnection* param = ObjectWrap::Unwrap<WebRtcConnection>(args[0]->ToObject());
  mcu::WebRtcConnection* wr = (mcu::WebRtcConnection*)param->me;

  mcu::MediaSource* ms = dynamic_cast<mcu::MediaSource*>(wr);
  me->setPublisher(ms);

  args.GetReturnValue().SetNull();
}

void OneToManyTranscoder::hasPublisher(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyTranscoder* obj = ObjectWrap::Unwrap<OneToManyTranscoder>(args.Holder());
  mcu::OneToManyTranscoder *me = (mcu::OneToManyTranscoder*)obj->me;

  bool p = true;

  if(me->publisher == NULL) {
    p = false;
  }
  
  return args.GetReturnValue().Set(p);
}

void OneToManyTranscoder::addSubscriber(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyTranscoder* obj = ObjectWrap::Unwrap<OneToManyTranscoder>(args.Holder());
  mcu::OneToManyTranscoder *me = (mcu::OneToManyTranscoder*)obj->me;

  WebRtcConnection* param = ObjectWrap::Unwrap<WebRtcConnection>(args[0]->ToObject());
  mcu::WebRtcConnection* wr = param->me;

  // get the param
  v8::String::Utf8Value param1(args[1]->ToString());

  // convert it to string
  std::string peerId = std::string(*param1);
  me->addSubscriber(wr, peerId);

  args.GetReturnValue().SetNull();
}

void OneToManyTranscoder::removeSubscriber(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyTranscoder* obj = ObjectWrap::Unwrap<OneToManyTranscoder>(args.Holder());
  mcu::OneToManyTranscoder *me = (mcu::OneToManyTranscoder*)obj->me;

  // get the param
  v8::String::Utf8Value param1(args[0]->ToString());

  // convert it to string
  std::string peerId = std::string(*param1);
  me->removeSubscriber(peerId);

  args.GetReturnValue().SetNull();
}

void OneToManyTranscoder::sendFIR(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);

  OneToManyTranscoder* obj = ObjectWrap::Unwrap<OneToManyTranscoder>(args.Holder());
  mcu::OneToManyTranscoder *me = (mcu::OneToManyTranscoder*)obj->me;

  me->publisher->sendFirPacket();
  
  args.GetReturnValue().SetNull();
}
