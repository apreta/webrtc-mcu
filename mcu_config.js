var config = {};

config.rabbit = {};
config.rpc = {};
config.mcuController = {};
config.cloudProvider = {};
config.mcu = {};
config.monitor = {};

config.rabbit.host = 'localhost';
config.rabbit.port = 5672;
config.rabbit.prefix = 'DEV';

/*config.rabbit.testMode = true;
config.rabbit.testRoom = '64';
config.rabbit.testHost = 'http://192.168.1.101:8080';*/

config.rpc.superserviceID = '51e445405a219d1a2fd33408';
config.rpc.superserviceKey = '28656';
config.rpc.testMcuController = 'localhost:8080';

//Use undefined to run clients without Stun 
//config.mcuController.stunServerUrl = 'stun:stun.l.google.com:19302';

//Use undefined to run clients without Turn
/*config.mcuController.turnServer = {};
config.mcuController.turnServer.url = 'turn:54.227.216.43:3478?transport=tcp';
config.mcuController.turnServer.username = 'kltest';
config.mcuController.turnServer.password = 'klpasswd';
*/

config.mcuController.warning_n_rooms = 15;
config.mcuController.limit_n_rooms = 20;
config.mcuController.interval_time_keepAlive = 1000;

/*config.sipController = {};
config.sipController.sipAddress = '192.168.1.101';
config.sipController.sipPort = 6060;

config.sipController.bridgeHost = '127.0.0.1';
config.sipController.bridgePort = 8021;
config.sipController.bridgePassword = 'ClueCon';
config.sipController.appPort = 8022;
*/

//STUN server IP address and port to be used by the server.
//if '' is used, the address is discovered locally
config.mcu.stunserver = '';
config.mcu.stunport = 0;

config.mcu.wsport = 8080;
config.mcu.wscert = null;
config.mcu.wsca = null;
config.mcu.wskey = null;

//note, this won't work with all versions of libnice. With 0 all the available ports are used
config.mcu.localaddress = "192.168.1.101";
config.mcu.minport = 0;
config.mcu.maxport = 0;

config.mcu.debug = '*';

config.cloudProvider.name = '';
//In Amazon Ec2 instances you can specify the zone host. By default is 'ec2.us-east-1a.amazonaws.com' 
config.cloudProvider.host = '';
config.cloudProvider.accessKey = '';
config.cloudProvider.secretAccessKey = '';

// Roles to be used by services
config.roles = {"presenter":["publish", "subscribe", "record"],
    "leader": ["publish", "subscribe"],
    "guest": ["publish", "subscribe"],
    "viewer":["subscribe"]
};

var module = module || {};
module.exports = config;
