#!/bin/bash

SCRIPT=`pwd`/$0
ROOT=`dirname $SCRIPT`
BUILD_DIR=$ROOT/build
DB_DIR="$BUILD_DIR"/db
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOT/mcu/build/mcu:$ROOT/mcu
export MCU_HOME=$ROOT/mcu/

ulimit -n 5000
ulimit -c unlimited

(
cd ./mcu_controller/mcuController
DEBUG=* node mcuController.js >mcu.log 2>&1 </dev/null &
#forever start -a -l forever.log -o mcu.log -e mcu-err.log mcuController.js
)

